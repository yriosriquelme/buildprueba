import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { hmrBootstrap } from 'hmr';
import Amplify from 'aws-amplify';
import { cognito } from 'config';
import { config } from 'process';

Amplify.configure({
    Auth: {
        mandatorySignId: true,
        region: cognito.REGION,
        userPoolId: cognito.USER_POOL_ID,
        userPoolWebClientId: cognito.APP_CLIENT_ID
    }
});

if ( environment.production )
{
    enableProdMode();
}

const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);

if ( environment.hmr )
{
    if ( module['hot'] )
    {
        hmrBootstrap(module, bootstrap);
    }
    else
    {
        console.error('HMR is not enabled for webpack-dev-server!');
        console.log('Are you using the --hmr flag for ng serve?');
    }
}
else
{
    bootstrap().catch(err => console.error(err));
}
