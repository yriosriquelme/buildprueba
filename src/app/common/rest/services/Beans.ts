export interface EmployeeTo {
	employeePk?: number;
	documentType?: number;
	documentNumber?: string;
	lastName?: string;
	name?: string;
	birthdate?: Date;
	businessEmail?: string;
	personalEmail?: string;
	businessPhone?: string;
	personalPhone?: string;
	adress?: string;
	maritalStatus?: number;
	childrenNumber?: number;
	gender?: number;
	employeeGrade?: string;
	admissionDate?: Date;
	endDate?: Date;
	checkTime?: string;
	closingHour?: Date;
	employeeStatus?: number;
	username?: string;
	password?: string;
	authToken?: string;
	codeResponse?: number;
	messageResponse?: string;
	state?: number;
	employeeCompleteName?: string;
}

export interface RequestCanonical {
	message?: string;
	state?: string;
	masterTo?: any;
	lstMaster?: Array<any>;
	parameterTo?: any;
	lstParameter?: Array<any>;
	employeeTo?: EmployeeTo;
	assistanceTo?: AssistanceTo;
	lstAssistanceTo?: Array<AssistanceTo>;
	permitsTo?: PermitsTo;
	lstPermits?: Array<PermitsTo>;
	permitsDayTo?: PermitsDayTo;
	lstPermitsDay?: Array<PermitsDayTo>;
	filesTo?: FilesTo;
	lstFilesTo?: Array<FilesTo>;
	vacationTo?: VacationTo;
	lstVacation?: Array<VacationTo>;
}

export interface ResponseCanonical {
	id?: number;
	message?: string;
	state?: string;
	error?: string;
	parameterTo?: ParameterTo;
	lstParameter?: Array<ParameterTo>;
	response?: number;
	employeeTo?: EmployeeTo;
	lstEmployeeTo?: Array<EmployeeTo>;
	assistanceTo?: AssistanceTo;
	lstAssistanceTo?: Array<AssistanceTo>;
	filesTo?: FilesTo;
	lstFilesTo?: Array<FilesTo>;
    vacationTo?: VacationTo;
	lstVacation?: Array<VacationTo>;
}

export interface ParameterTo {
	parameterPk: number;
	name: string;
	order: number;
	codeCategory: number;
	masterFk: number;
	state: number;
}

export interface AssistanceTo {
	assistancePK?: number;
	assistanceDay?: Date;
	entryTime?: Date;
	depatureTime?: Date;
	hoursWorked?: Date;
	state?: number;
	employeeFk?: number;
	employeeName?: string;
	employeeLastName?: string;
}

export interface FilesTo {
	filesPK?: number;
	descdocu?: string;
	descfile?: string;
	uri?: string;
	state?: number;
	employeeFk?: number;
}

export interface PermitsTo {
	permitsPK?: number;
	support?: string;
	description?: string;
	reason?: string;
	name?: string;
	lastName?: string;
	startDate?: Date;
	endDate?: Date;
	permitStatus?: number;
	affectsHours?: boolean;
	employeeFk?: number;
}

export interface PermitsDayTo {
	permitsDayPk?: number;
	permitsDayDate?: Date;
	permitsDayEntryTime?: Date;
	permitsDayDepatureTime?: Date;
	description?: string;
	permitsPk?: number;
}

export interface VacationTo{
	vacationPk?: number;	
	name?: string;	
	startVacation?: Date;
	endVacation?: Date;
	dayRequest?: number;
	employeeFk?: number;
	state?: number;
}