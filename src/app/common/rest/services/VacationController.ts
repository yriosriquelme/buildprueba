import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
providedIn: 'root'
})
export class VacationController{
  private url: string;
  private header: HttpHeaders;
  constructor (private httpClient: HttpClient, private oauth: OAuthService)  {
    this.url = oauth.servidor + '/vacationResource';
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
  }

      createVacation(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/createVacation';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }
      
    getListVacation(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/getListVacation';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    updateVacation(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/updateVacation';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }
  

  }