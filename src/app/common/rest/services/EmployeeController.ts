import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OAuthService } from '../../oauth.service';
import * as beans from './Beans';
import * as moment from 'moment';
@Injectable({
providedIn: 'root'
})
export class EmployeeController   {
  private url: string;
  private header: HttpHeaders;
  constructor (private httpClient: HttpClient, private oauth: OAuthService)  {
    this.url = oauth.servidor + '/employeeResource';
    this.header = new HttpHeaders({ 'Content-Type': 'application/json' });
    // Date.prototype.toJSON = function () { return moment(this).format('DD/MM/YYYY HH:mm:ss'); };
  }

    getListEmployee(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/getListEmployee';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    createEmployee(arg0: beans.RequestCanonical): Observable<any> {
        const metodo = this.url + '/createEmployee';
        // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
        console.log(JSON.stringify(arg0));
        
        return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
    }

    updateEmployee(arg0: beans.RequestCanonical): Observable<any> {
      const metodo = this.url + '/updateEmployee';
      // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
      console.log(JSON.stringify(arg0));
      
      return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header});
  }

  findByUsername(arg0: string): Observable<any> {
    const metodo = this.url + '/findByUsername';
    // return this.httpClient.post(metodo, JSON.stringify(arg0), {headers: this.header,  withCredentials: true });    }
    console.log(JSON.stringify(arg0));
    
    return this.httpClient.post(metodo, arg0, {headers: this.header});
}

  }