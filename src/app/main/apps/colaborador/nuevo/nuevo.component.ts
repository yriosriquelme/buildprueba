import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { DialogDeleteComponent } from './dialog-delete/dialog.delete.component';
import { FilesController } from 'app/common/rest/services/FilesController';
import { EmployeeTo, ParameterTo, FilesTo, RequestCanonical } from 'app/common/rest/services/Beans';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { ParameterController } from 'app/common/rest/services/ParameterController';

@Component({
    selector: 'nuevoEmpleado',
    templateUrl: './nuevo.component.html',
    styleUrls: ['./nuevo.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NewEmployeComponent implements OnInit, OnDestroy {
    botonDescp: string = "";
    user1: string = "";
    user2: string = "";
    user3: string = "";
    usuario: string = "";
    tipDocumento1: number;
    numDocumento1: string = "";
    apePaterno1: string = "";
    apeMaterno1: string = "";
    nombres1: string = "";
    fecNacimiento1: Date;
    corElectronico1: string = "";
    telefono1: string = "";
    corElectronico2: string = "";
    telefono2: string = "";
    direccion1: string = "";
    estCivil1: number;
    numHijos1: number = 0;
    genero1: number;
    fecIngreso1: Date;
    fecTermino1: Date;
    puesto1: string = "";
    horario1: string ;
    horario2: Date ;
    estado1: number;

    
    documento: string = "";
    fileToUpload: string = "Ningún archivo seleccionado";
    fileToUpload1: string = "";
    uri: string = "";

    nuevoTipoDocumento1 = [
        { tipodocumento1: 'DNI' },
        { tipodocumento1: 'Pasaporte' },
        { tipodocumento1: 'Carnet' }
    ];

    nuevoPuesto1 = [
        { puesto: 'Software Developer' },
        { puesto: 'QA' }
    ];

    nuevoHorario1 = [
        { horario: '8:00AM - 5:00PM' },
        { horario: '9:00AM - 6:00PM' },
        { horario: '10:00AM - 7:00PM' }
    ];

    nuevoEstado1 = [
        { estado: 'Activo' },
        { estado: 'Inactivo' }
    ];

    nuevoTipoDocumento2 = [
        { tipodocumento2: 'DNI' },
        { tipodocumento2: 'Contrato' },
        { tipodocumento2: 'Prorrogas' },
        { tipodocumento2: 'Préstamo' },
        { tipodocumento2: 'DJ' },
        { tipodocumento2: 'Solicitud' }
    ];

    nuevoEmpleado: Empleado = null;
    actualizarEmpleado: EmployeeTo = null;
    lstArchivos: Archivos = null;
    public myClass: boolean = false;
    tabIndex: number = 0;
    lstParameter: ParameterTo;
    tipeDocument: any = [{}];
    tipeDocument2: any = [{}];
    tipeAttach: any = [{}];
    tipeMaritalStatus: any = [{}];
    tipeGender: any = [{}];
    tipeMarket: any = [{}];
    lstTimeTable: any = [{}];
    lstState: any = [{}];
    varDoc: any = {};
    varLastName: string;
    columnas: string[] = ['documento', 'archivo', 'descargar', 'eliminar'];

    datos: MatTableDataSource<Archivos>;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    selection = new SelectionModel<Archivos>(true, []);

    // google maps zoom level
    zoom: number = 8;

    // initial center position for the map
    lat: number = 51.673858;
    lng: number = 7.815982;

    markers: marker[] = [
        {
            lat: 51.673858,
            lng: 7.815982,
            label: 'A',
            draggable: true
        },
        {
            lat: 51.373858,
            lng: 7.215982,
            label: 'B',
            draggable: false
        },
        {
            lat: 51.723858,
            lng: 7.895982,
            label: 'C',
            draggable: true
        }
    ]

    public formGroup1: FormGroup;
    public formGroup2: FormGroup;

    constructor(
        private formBuilder1: FormBuilder,
        private formBuilder2: FormBuilder,
        private rutaActiva: ActivatedRoute,
        private rutaActiva1: ActivatedRoute,
        private router: Router,
        public dialog: MatDialog,
        private _filesCtrl: FilesController,
        private _employeeCtrl: EmployeeController,
        private _lstipeDocument: ParameterController,
        private _lstAttach: ParameterController,
    ) {
        const dato = ELEMENT_DATA;
        this.datos = new MatTableDataSource(dato);
        this.rutaActiva.queryParams.subscribe(params => {
            this.botonDescp = params['boton'];
        });


        const typeDoc: any = {};
        typeDoc.lstMaster = [null, null];
        typeDoc.lstParameter = [null, null];
        typeDoc.masterTo = {};
        typeDoc.message = '';
        typeDoc.parameterTo = {
            masterFk: 1,
            state: 1
        };
        typeDoc.state = '';
        this._lstipeDocument.getListParameters(typeDoc).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.tipeDocument = this.lstParameter;
                console.log(this.tipeDocument);
            },
            error => console.log(JSON.stringify(error))
        );
        //this.tipDocumento1 =  this.tipeDocument2;
        const maritalStatus: any = {};
        maritalStatus.lstMaster = [null, null];
        maritalStatus.lstParameter = [null, null];
        maritalStatus.masterTo = {};
        maritalStatus.message = '';
        maritalStatus.parameterTo = {
            masterFk: 2,
            state: 1
        };
        maritalStatus.state = '';
        this._lstipeDocument.getListParameters(maritalStatus).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.tipeMaritalStatus = this.lstParameter;
                console.log(this.tipeMaritalStatus);
            },
            error => console.log(JSON.stringify(error))
        );

        const gender: any = {};
        gender.lstMaster = [null, null];
        gender.lstParameter = [null, null];
        gender.masterTo = {};
        gender.message = '';
        gender.parameterTo = {
            masterFk: 5,
            state: 1
        };
        gender.state = '';
        this._lstipeDocument.getListParameters(gender).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.tipeGender = this.lstParameter;
                console.log(this.tipeGender);
            },
            error => console.log(JSON.stringify(error))
        );

        const puesto: any = {};
        puesto.lstMaster = [null, null];
        puesto.lstParameter = [null, null];
        puesto.masterTo = {};
        puesto.message = '';
        puesto.parameterTo = {
            masterFk: 6,
            state: 1
        };
        puesto.state = '';
        this._lstipeDocument.getListParameters(puesto).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.tipeMarket = this.lstParameter;
                console.log(this.tipeMarket);
            },
            error => console.log(JSON.stringify(error))
        );

        const adjunto: any = {};
        adjunto.lstMaster = [null, null];
        adjunto.lstParameter = [null, null];
        adjunto.masterTo = {};
        adjunto.message = '';
        adjunto.parameterTo = {
            masterFk: 11,
            state: 1
        };
        adjunto.state = '';
        this._lstAttach.getListParameters(adjunto).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.tipeAttach = this.lstParameter;
                console.log(this.tipeAttach);
            },
            error => console.log(JSON.stringify(error))
        );

        const horario: any = {};
        horario.lstMaster = [null, null];
        horario.lstParameter = [null, null];
        horario.masterTo = {};
        horario.message = '';
        horario.parameterTo = {
            masterFk: 10,
            state: 1
        };
        horario.state = '';
        this._lstipeDocument.getListParameters(horario).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.lstTimeTable = this.lstParameter;
                console.log(this.lstTimeTable);
            },
            error => console.log(JSON.stringify(error))
        );

        const estado: any = {};
        estado.lstMaster = [null, null];
        estado.lstParameter = [null, null];
        estado.masterTo = {};
        estado.message = '';
        estado.parameterTo = {
            masterFk: 7,
            state: 1
        };
        estado.state = '';
        this._lstipeDocument.getListParameters(estado).subscribe(
            data => {
                this.lstParameter = data.lstParameter;
                this.lstState = this.lstParameter;
                console.log(this.lstState);
            },
            error => console.log(JSON.stringify(error))
        );
        this.datos.paginator = this.paginator;
        this.datos.sort = this.sort;

        this.rutaActiva1.queryParams.subscribe(params => {
            this.actualizarEmpleado = JSON.parse(params['empleado']);
            this.botonDescp = params['boton'];
        });
        if (this.actualizarEmpleado != null) {
            console.log(this.actualizarEmpleado);

            this.usuario = this.actualizarEmpleado.username;
            //this.actualizarEmpleado.documentType.toString() == "DNI";
            //this.varDoc = this.actualizarEmpleado.documentType.toString();
            //this.varDoc = "DNI";
            //console.log(this.varDoc);
            this.tipDocumento1 = this.actualizarEmpleado.documentType;

            //Number.toString(this.actualizarEmpleado.documentType);

            this.numDocumento1 = this.actualizarEmpleado.documentNumber;
            if (this.actualizarEmpleado.lastName != null) {
                if (this.actualizarEmpleado.lastName.indexOf(" ") === -1) {
                    this.apePaterno1 = this.actualizarEmpleado.lastName;
                } else {
                    var varLastName = this.actualizarEmpleado.lastName.split(' ');
                    this.apePaterno1 = varLastName[0];
                    this.apeMaterno1 = varLastName[1];
                }
            }


            this.nombres1 = this.actualizarEmpleado.name;
            this.fecNacimiento1 = this.actualizarEmpleado.birthdate;
            this.corElectronico1 = this.actualizarEmpleado.personalEmail;
            this.telefono1 = this.actualizarEmpleado.personalPhone;
            this.corElectronico2 = this.actualizarEmpleado.businessEmail;
            this.telefono2 = this.actualizarEmpleado.businessPhone;
            this.direccion1 = this.actualizarEmpleado.adress;
            this.estCivil1 = this.actualizarEmpleado.maritalStatus;
            this.numHijos1 = this.actualizarEmpleado.childrenNumber;
            this.genero1 = this.actualizarEmpleado.gender;
            this.fecIngreso1 = this.actualizarEmpleado.admissionDate;
            this.fecTermino1 = this.actualizarEmpleado.endDate;
            if (this.actualizarEmpleado.employeeGrade != null) {
                this.puesto1 = this.actualizarEmpleado.employeeGrade;
            }
            this.horario1 =this.actualizarEmpleado.checkTime;
            //this.horario1 =  this.horario2.toDateString();
            this.estado1 = this.actualizarEmpleado.state;
            //this.datos.data = this.actualizarEmpleado.lstArchivos;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.buildForm1();
        this.buildForm2();
        this.actualizarEmpleado;
        console.log(this.actualizarEmpleado);
    }

    private buildForm1() {
        const usuario = "";
        const tipDocumento1 = "";
        const numDocumento1 = "";
        const apePaterno1 = "";
        const apeMaterno1 = "";
        const nombres1 = "";
        const fecNacimiento1 = "";
        const corElectronico1 = "";
        const telefono1 = "";
        const corElectronico2 = "";
        const telefono2 = "";
        const direccion1 = "";
        const estCivil1 = "";
        const numHijos1 = "";
        const genero1 = "";
        const fecIngreso1 = "";
        const fecTermino1 = "";
        const puesto1 = "";
        const horario1 = "";
        const estado1 = "";

        this.formGroup1 = this.formBuilder1.group({
            usuario: usuario,
            tipDocumento1: tipDocumento1,
            numDocumento1: numDocumento1,
            apePaterno1: apePaterno1,
            apeMaterno1: apeMaterno1,
            nombres1: nombres1,
            fecNacimiento1: fecNacimiento1,
            corElectronico1: corElectronico1,
            telefono1: telefono1,
            corElectronico2: corElectronico2,
            telefono2: telefono2,
            direccion1: direccion1,
            estCivil1: estCivil1,
            numHijos1: numHijos1,
            genero1: genero1,
            fecIngreso1: fecIngreso1,
            fecTermino1: fecTermino1,
            puesto1: puesto1,
            horario1: horario1,
            estado1: estado1
        });
    }

    private buildForm2() {
        const documento = "";
        const fileToUpload1 = "";
        this.formGroup2 = this.formBuilder2.group({
            documento: documento,
            fileToUpload1: fileToUpload1
        });
    }

    update1(value: string) {
        this.user1 = value.substring(0, 1).toLocaleLowerCase();
    }
    update2(value: string) {
        this.user2 = value.substring(0, 1).toLocaleLowerCase();
    }
    update3(value: string) {
        var a = value.split(" ", 1);
        this.user3 = this.user1 + this.user2 + a[0].toLocaleLowerCase();
        this.usuario = this.user3;
    }

    regresar() {
        this.router.navigate(['/apps/colaborador/buscar']);
    }

    selectMat(a) {
        console.log(a.index)
    }

    siguienteNuevoEmpleado1() {
        this.tabIndex = 1;


    }

    limpiarNuevoEmpleado1() {
        this.buildForm1();
    }

    handleFileInput(files: FileList) {
        if (files != null) {
            var reader = new FileReader();
            var evt = files.item(0);
            this.fileToUpload = files.item(0).name;
            this.fileToUpload1 = files.item(0).name;
            reader.onload = this.onLoadCallback.bind(this);
            reader.readAsBinaryString(evt);
            this.myClass = !this.myClass;
        }
    }

    onLoadCallback = (event) => {
        this.uri = (btoa((event.target as FileReader).result.toString()));
    }

    ngOnDestroy(): void {

    }

    clickedMarker(label: string, index: number) {
        console.log(`clicked the marker: ${label || index}`)
    }
    enableDisableRule() {
        this.myClass = !this.myClass;

    }
    mapClicked($event: MouseEvent) {
        this.markers.push({
            lat: $event.coords.lat,
            lng: $event.coords.lng,
            draggable: true
        });
    }

    markerDragEnd(m: marker, $event: MouseEvent) {
        console.log('dragEnd', m, $event);
    }

    agregarNuevoEmpleado1() {
        const requestCanonical: RequestCanonical = {};
        const employeeTo: any = {};
        /*employeeTo.employeePk = 2;
        requestCanonical.employeeTo = employeeTo;
        this._filesCtrl.getListFilesEmployee(requestCanonical).subscribe(
            data => {
                console.log(data);
            },
            error => console.log(JSON.stringify(error))
        );*/
        /*this._filesCtrl.getListFiles(requestCanonical).subscribe(
            data => {
                console.log(data);
            },
            error => console.log(JSON.stringify(error))
        );*/
        const filesTo: any = {};
        filesTo.descdocu = this.documento;
        filesTo.descfile = this.fileToUpload1;
        filesTo.uri = this.uri;
        filesTo.state = 1;
        filesTo.employeeFk = 2;
        requestCanonical.filesTo = filesTo;
        console.log(requestCanonical);
        this._filesCtrl.createFiles(requestCanonical).subscribe(
            data => {
                console.log(data);
            },
            error => console.log(JSON.stringify(error))
        );
        /*filesTo.filesPK = 54;
        filesTo.descdocu = 'clin';
        filesTo.descfile = 'clin.pdf';
        requestCanonical.filesTo = filesTo;
        console.log(requestCanonical);
        this._filesCtrl.updateFiles(requestCanonical).subscribe(
            data => {
                console.log(data);
            },
            error => console.log(JSON.stringify(error))
        );*/
        if (this.documento != null && this.documento != "") {
            this.lstArchivos = { documento: this.documento, archivo: this.fileToUpload1 };
            this.datos.data.push(this.lstArchivos);
            this.datos._updateChangeSubscription();
            this.documento = "";
            this.fileToUpload = "Ningún archivo seleccionado"
            this.fileToUpload1 = "";
        }
    }

    eliminar(i: number) {
        this.openDialog(i);

    }

    onCreateEmploye() {
        const usuario = this.formGroup1.get('usuario').value;
        const tipDocumento1 = this.formGroup1.get('tipDocumento1').value;
        const numDocumento1 = this.formGroup1.get('numDocumento1').value;
        const apePaterno1 = this.formGroup1.get('apePaterno1').value;
        const apeMaterno1 = this.formGroup1.get('apeMaterno1').value;
        const nombres1 = this.formGroup1.get('nombres1').value;
        const fecNacimiento1 = this.formGroup1.get('fecNacimiento1').value;
        const corElectronico1 = this.formGroup1.get('corElectronico1').value;
        const telefono1 = this.formGroup1.get('telefono1').value;
        const corElectronico2 = this.formGroup1.get('corElectronico2').value;
        const telefono2 = this.formGroup1.get('telefono2').value;
        const direccion1 = this.formGroup1.get('direccion1').value;
        const estCivil1 = this.formGroup1.get('estCivil1').value;
        const numHijos1 = this.formGroup1.get('numHijos1').value;
        const genero1 = this.formGroup1.get('genero1').value;
        const fecIngreso1 = this.formGroup1.get('fecIngreso1').value;
        const fecTermino1 = this.formGroup1.get('fecTermino1').value;
        const puesto1 = this.formGroup1.get('puesto1').value;
        const horario1 = this.formGroup1.get('horario1').value;
        const estado1 = this.formGroup1.get('estado1').value;

        this.usuario = usuario;
        this.tipDocumento1 = tipDocumento1;
        this.numDocumento1 = numDocumento1;
        this.apePaterno1 = apePaterno1;
        this.apeMaterno1 = apeMaterno1;
        this.nombres1 = nombres1;
        this.fecNacimiento1 = fecNacimiento1;
        this.corElectronico1 = corElectronico1;
        this.telefono1 = telefono1;
        this.corElectronico2 = corElectronico2;
        this.telefono2 = telefono2;
        this.direccion1 = direccion1;
        this.estCivil1 = estCivil1;
        this.numHijos1 = numHijos1;
        this.genero1 = genero1;
        this.fecIngreso1 = fecIngreso1;
        this.fecTermino1 = fecTermino1;
        if (puesto1 == "") {
            this.puesto1 = null;
        } else {
            this.puesto1 = puesto1;
        }
        this.horario1 = horario1;
        this.estado1 = estado1;

        const employe: any = {};
        employe.lstMaster = [null, null];
        employe.lstParameter = [null, null];
        employe.masterTo = {};
        employe.message = '';
        employe.parameterTo = {};
        employe.state = '';

        employe.employeeTo = {
            employeeStatus: 1,
            username: this.usuario,
            documentType: this.tipDocumento1,
            documentNumber: this.numDocumento1,
            name: this.nombres1,
            lastName: this.apePaterno1 + " " + this.apeMaterno1,
            birthdate: this.fecNacimiento1,
            personalEmail: this.corElectronico1,
            personalPhone: this.telefono1,
            businessEmail: this.corElectronico2,
            businessPhone: this.telefono2,
            adress: this.direccion1,
            maritalStatus: this.estCivil1,
            childrenNumber: this.numHijos1,
            gender: this.genero1,
            admissionDate: this.fecIngreso1,
            endDate: this.fecTermino1,
            employeeGrade: this.puesto1,
            checkTime: this.horario1,
            state: this.estado1
        };
        this._employeeCtrl.createEmployee(employe).subscribe(
            data => {
                console.log(data);
                if(data.message=="SUCCESS"){
                    this.router.navigate(['/apps/colaborador/buscar']);
                }
            },
            error => console.log(JSON.stringify(error))

        );
        
    }

    onChangeEmployee(){
        const usuario = this.formGroup1.get('usuario').value;
        const tipDocumento1 = this.formGroup1.get('tipDocumento1').value;
        const numDocumento1 = this.formGroup1.get('numDocumento1').value;
        const apePaterno1 = this.formGroup1.get('apePaterno1').value;
        const apeMaterno1 = this.formGroup1.get('apeMaterno1').value;
        const nombres1 = this.formGroup1.get('nombres1').value;
        const fecNacimiento1 = this.formGroup1.get('fecNacimiento1').value;
        const corElectronico1 = this.formGroup1.get('corElectronico1').value;
        const telefono1 = this.formGroup1.get('telefono1').value;
        const corElectronico2 = this.formGroup1.get('corElectronico2').value;
        const telefono2 = this.formGroup1.get('telefono2').value;
        const direccion1 = this.formGroup1.get('direccion1').value;
        const estCivil1 = this.formGroup1.get('estCivil1').value;
        const numHijos1 = this.formGroup1.get('numHijos1').value;
        const genero1 = this.formGroup1.get('genero1').value;
        const fecIngreso1 = this.formGroup1.get('fecIngreso1').value;
        const fecTermino1 = this.formGroup1.get('fecTermino1').value;
        const puesto1 = this.formGroup1.get('puesto1').value;
        const horario1 = this.formGroup1.get('horario1').value;
        const estado1 = this.formGroup1.get('estado1').value;

        this.usuario = usuario;
        this.tipDocumento1 = tipDocumento1;
        this.numDocumento1 = numDocumento1;
        this.apePaterno1 = apePaterno1;
        this.apeMaterno1 = apeMaterno1;
        this.nombres1 = nombres1;
        this.fecNacimiento1 = fecNacimiento1;
        this.corElectronico1 = corElectronico1;
        this.telefono1 = telefono1;
        this.corElectronico2 = corElectronico2;
        this.telefono2 = telefono2;
        this.direccion1 = direccion1;
        this.estCivil1 = estCivil1;
        this.numHijos1 = numHijos1;
        this.genero1 = genero1;
        this.fecIngreso1 = fecIngreso1;
        this.fecTermino1 = fecTermino1;
        if (puesto1 == "") {
            this.puesto1 = null;
        } else {
            this.puesto1 = puesto1;
        }
        this.horario1 = horario1;
        this.estado1 = estado1;

        const employe: any = {};
        employe.lstMaster = [null, null];
        employe.lstParameter = [null, null];
        employe.masterTo = {};
        employe.message = '';
        employe.parameterTo = {};
        employe.state = '';

        employe.employeeTo = {
            employeeStatus: 1,
            username: this.usuario,
            documentType: this.tipDocumento1,
            documentNumber: this.numDocumento1,
            name: this.nombres1,
            lastName: this.apePaterno1 + " " + this.apeMaterno1,
            birthdate: this.fecNacimiento1,
            personalEmail: this.corElectronico1,
            personalPhone: this.telefono1,
            businessEmail: this.corElectronico2,
            businessPhone: this.telefono2,
            adress: this.direccion1,
            maritalStatus: this.estCivil1,
            childrenNumber: this.numHijos1,
            gender: this.genero1,
            admissionDate: this.fecIngreso1,
            endDate: this.fecTermino1,
            employeeGrade: this.puesto1,
            checkTime: this.horario1,
            state: this.estado1
        };
        this._employeeCtrl.updateEmployee(employe).subscribe(
            data => {
                console.log(data);
                if(data.message=="SUCCESS"){
                    this.router.navigate(['/apps/colaborador/buscar']);
                }
            },
            error => console.log(JSON.stringify(error))

        );

    }
    agregarNuevoEmpleado2() {

        if(this.botonDescp == "Actualizar") {
            this.onChangeEmployee();
        }else{
            this.onCreateEmploye();
        }
    }

    /*agregarNuevoEmpleado2() {
        if (this.botonDescp == "Actualizar") {
            this.router.navigate(['/apps/colaborador/buscar'], {
                queryParams: {
                    usuario: this.usuario, tipoDocumento: this.tipDocumento1, numDocumento: this.numDocumento1,
                    apePaterno: this.apePaterno1, apeMaterno: this.apeMaterno1, nombres: this.nombres1,
                    fecNacimiento: this.fecNacimiento1, corElectronicoPer: this.corElectronico1, telefonoPer: this.telefono1,
                    corElectronicoEmp: this.corElectronico2, telefonoEmp: this.telefono2, direccion: this.direccion1, estCivil: this.estCivil1,
                    numHijos: this.numHijos1, genero: this.genero1, fecIngreso: this.fecIngreso1, fecTermino: this.fecTermino1,
                    puesto: this.puesto1, horario: this.horario1, estado: this.estado1, lstArchivos: this.datos.data
                }
            });
        } else {
            if (this.tipDocumento1 != null && this.tipDocumento1 != "") {
                this.nuevoEmpleado = {
                    usuario: this.usuario, tipoDocumento: this.tipDocumento1, numDocumento: this.numDocumento1,
                    apePaterno: this.apePaterno1, apeMaterno: this.apeMaterno1, nombres: this.nombres1,
                    fecNacimiento: this.fecNacimiento1, corElectronicoPer: this.corElectronico1, telefonoPer: this.telefono1,
                    corElectronicoEmp: this.corElectronico2, telefonoEmp: this.telefono2, direccion: this.direccion1, estCivil: this.estCivil1,
                    numHijos: this.numHijos1, genero: this.genero1, fecIngreso: this.fecIngreso1, fecTermino: this.fecTermino1,
                    puesto: this.puesto1, horario: this.horario1, estado: this.estado1, lstArchivos: this.datos.data
                };
                console.log(this.nuevoEmpleado);
                this.router.navigate(['/apps/colaborador/buscar'], {
                    queryParams: {
                        usuario: this.usuario, tipoDocumento: this.tipDocumento1, numDocumento: this.numDocumento1,
                        apePaterno: this.apePaterno1, apeMaterno: this.apeMaterno1, nombres: this.nombres1,
                        fecNacimiento: this.fecNacimiento1, corElectronicoPer: this.corElectronico1, telefonoPer: this.telefono1,
                        corElectronicoEmp: this.corElectronico2, telefonoEmp: this.telefono2, direccion: this.direccion1, estCivil: this.estCivil1,
                        numHijos: this.numHijos1, genero: this.genero1, fecIngreso: this.fecIngreso1, fecTermino: this.fecTermino1,
                        puesto: this.puesto1, horario: this.horario1, estado: this.estado1, lstArchivos: this.datos.data
                    }
                });
            }
        }
    }*/

    openDialog(i: number) {
        // =====================================
        const dialogRef = this.dialog.open(DialogDeleteComponent, {
            // width: '250px',
            height: '250px',
            width: '400px',
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log(`dialog result: ${result}`);
            if (result == "true") {
                this.datos.data.splice(i, 1);
                this.datos._updateChangeSubscription();
            } else {
                console.log("false");
            }

        });

    }
    limpiarNuevoEmpleado2() {
        this.buildForm2();
        //this.documento = "";
        this.fileToUpload = "Ningún archivo seleccionado"
        this.fileToUpload1 = "";
    }


}

interface Archivos {
    documento: string;
    archivo: string;
}

interface Empleado {
    usuario: string;
    tipoDocumento: string;
    numDocumento: string;
    apePaterno: string;
    apeMaterno: string;
    nombres: string;
    fecNacimiento: string;
    corElectronicoPer: string;
    telefonoPer: string;
    corElectronicoEmp: string;
    telefonoEmp: string;
    direccion: string;
    estCivil: string;
    numHijos: number;
    genero: string;
    fecIngreso: string;
    fecTermino: string;
    puesto: string;
    horario: string;
    estado: string;
    lstArchivos: Array<Archivos>;
}

interface marker {
    lat: number;
    lng: number;
    label?: string;
    draggable: boolean;
}

const ELEMENT_DATA: Archivos[] = [
    /*{ documento: 'DNI', archivo: 'DNI_46360431.JPG' },
    { documento: 'Prorroga', archivo: 'Prorroga_46360431.PNG' },
    { documento: 'Prorroga1', archivo: 'Prorroga1_46360431.PDF' },
    { documento: 'Contrato', archivo: 'Contrato_46360431.PDF' }*/
];