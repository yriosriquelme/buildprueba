import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialog-delete',
  templateUrl: './dialog.delete.component.html',
  styleUrls: ['./dialog.delete.component.scss']
})
export class DialogDeleteComponent implements OnInit {
   
  constructor(public dialogRef: MatDialogRef<DialogDeleteComponent>,) {
      
  }

  ngOnInit() {
   
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}
