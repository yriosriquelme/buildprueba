import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';
import { ParameterController } from 'app/common/rest/services/ParameterController';
import { EmployeeTo, ParameterTo, ResponseCanonical } from 'app/common/rest/services/Beans';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { ReplaySubject, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'buscarEmpleado',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchEmployeComponent implements OnInit, OnDestroy {
  tipoDocumento: string = "";
  documento: string = "";
  estado: string = "";
  nombres: string = "";

  tipDocumento = [
    { tipDocumento: 'DNI' },
    { tipDocumento: 'Pasaporte' },
    { tipDocumento: 'Carnet' }
  ];

  estados = [
    { estado: 'Activo' },
    { estado: 'Inactivo' }
  ];

  nuevoEmpleado = {
    usuario: '', tipoDocumento: '', numDocumento: '', apePaterno: '', apeMaterno: '', nombres: '',
    fecNacimiento: '', corElectronicoPer: '', telefonoPer: '',
    corElectronicoEmp: '', telefonoEmp: '', direccion: '', estCivil: '',
    numHijos: 0, genero: '', fecIngreso: '', fecTermino: '',
    puesto: '', horario: '', estado: '', lstArchivos: []
  };

  public formGroup: FormGroup;
  //private subscription: Subscription;

  columnas: string[] = ['tipoDocumento', 'documento', 'nombre', 'puesto', 'estado', 'actions'];
  objectSelect: any[] = [];
  rowChecked: boolean = false;
  modHidden: boolean = true;
  actualizarEmpleado: Empleado = null;
  lstParameter: ParameterTo;
  lstEmployeeTo: EmployeeTo;
  lstEmployeeTo2: EmployeeTo;
  employeeTo: ResponseCanonical;
  parameterPk: number;
  //datos: MatTableDataSource<Empleado>;
  datos: any = [{}];
  datos2: any = {};
  filtro: any = {};
  filtro2: any = {};
  filtro3: any = {};
  filtroDoc: any = {};
  filtroEstado: any = {};
  filtroEmployee: any = {};
  tipeDocument: any = [{}];
  tipeEstado: any = [{}];
  tipeEmployee: any = [{}];
  tipeEmployee1: any = [{}];
  tipeEmployee2: any = [{}];
  tipeEmployee3: any = [{}];
  tipeMarket: any = [{}];
  tipeDoc: any = {};
  //lstEmploye:any = {};
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<EmployeeTo>(true, []);
  //private _unsubscribeAll: Subject<any>;
  constructor(
    private formBuilder: FormBuilder,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private _lstEstado: ParameterController,
    private _lstipeDocument: ParameterController,
    private _lstMarket: ParameterController,
    private _employeeCtrl: EmployeeController,

  ) {
    
    //const dato = ELEMENT_DATA;
    //this._unsubscribeAll = new Subject();
    /*this.rutaActiva.queryParams.subscribe(params => {
      this.nuevoEmpleado.usuario = params['usuario'];
      this.nuevoEmpleado.tipoDocumento = params['tipoDocumento'];
      this.nuevoEmpleado.numDocumento = params['numDocumento'];
      this.nuevoEmpleado.apePaterno = params['apePaterno'];
      this.nuevoEmpleado.apeMaterno = params['apeMaterno'];
      this.nuevoEmpleado.nombres = params['nombres'];
      this.nuevoEmpleado.fecNacimiento = params['fecNacimiento'];
      this.nuevoEmpleado.corElectronicoPer = params['corElectronicoPer'];
      this.nuevoEmpleado.telefonoPer = params['telefonoPer'];
      this.nuevoEmpleado.corElectronicoEmp = params['corElectronicoEmp'];
      this.nuevoEmpleado.telefonoEmp = params['telefonoEmp'];
      this.nuevoEmpleado.direccion = params['direccion'];
      this.nuevoEmpleado.estCivil = params['estCivil'];
      this.nuevoEmpleado.numHijos = params['numHijos'];
      this.nuevoEmpleado.genero = params['genero'];
      this.nuevoEmpleado.fecIngreso = params['fecIngreso'];
      this.nuevoEmpleado.fecTermino = params['fecTermino'];
      this.nuevoEmpleado.puesto = params['puesto'];
      this.nuevoEmpleado.horario = params['horario'];
      this.nuevoEmpleado.estado = params['estado'];
      this.nuevoEmpleado.lstArchivos = params['lstArchivos'];
    });
    if (this.nuevoEmpleado.tipoDocumento != "" && this.nuevoEmpleado.tipoDocumento != null) {
      console.log(this.nuevoEmpleado);
      this.datos.data.push(this.nuevoEmpleado);
    }*/

    //let project = new ReplaySubject(1);
    
   /* const a: any = {};
    a.lstMaster = [null, null];
    a.lstParameter = [null, null];
    a.masterTo = {};
    a.message = '';
    a.parameterTo = {
      masterFk: 1,
      state: 1
    };
    a.state = '';
    this._lstipeDocument.getListParameters(a).subscribe(
      data => {
        this.lstParameter = data.lstParameter;
        this.filtro.lstParameter = data.lstParameter;
        this.tipeDocument = this.lstParameter;
        //el parameter pk lo mando al document type del empleado
        console.log(this.filtro.lstParameter);
        console.log(this.tipeDocument);
      },
      error => console.log(JSON.stringify(error))
    );*/

    /*for (var i = 0; i < this.tipeDocument.length; i++) {
      this.tipeDocument[i].name = 1;
     }*/
    /*const estado: any = {};
    estado.lstMaster = [null, null];
    estado.lstParameter = [null, null];
    estado.masterTo = {};
    estado.message = '';
    estado.parameterTo = {
      masterFk: 7,
      state: 1
    };
    estado.state = '';
    this._lstEstado.getListParameters(estado).subscribe(
      data => {
        this.lstParameter = data.lstParameter;
        this.filtro2.lstParameter = data.lstParameter;
        this.tipeEstado = this.lstParameter;
        console.log(this.filtro2.lstParameter);
        console.log(this.tipeEstado);
      },
      error => console.log(JSON.stringify(error))
    );*/
    /*const puesto: any = {};
    puesto.lstMaster = [null, null];
    puesto.lstParameter = [null, null];
    puesto.masterTo = {};
    puesto.message = '';
    puesto.parameterTo = {
      masterFk: 6,
      state: 1
    };
    puesto.state = '';
    this._lstMarket.getListParameters(puesto).subscribe(
      data => {
        this.lstParameter = data.lstParameter;
        this.tipeMarket = this.lstParameter;
        console.log(this.tipeMarket);
      },
      error => console.log(JSON.stringify(error))
    );*/

    this.datos = new MatTableDataSource();

  }

  ngOnInit(): void {
    this.buildForm();
    //this.datos.paginator = this.paginator;
    //this.datos.filterPredicate = this.getFilterPredicate()
    
    let doc = JSON.parse(localStorage.getItem('currentDoc'));
    let state = JSON.parse(localStorage.getItem('currentState'));
    let market = JSON.parse(localStorage.getItem('currentMarket'));
    console.log(doc);
    console.log(state);
    console.log(market);
    this.tipeDocument = doc;
    this.tipeEstado = state;
    this.tipeMarket = market;
    let employe: any = {};
    
    //var employe= new employe();
    employe.lstMaster = [null, null];
    employe.lstParameter = [null, null];
    employe.masterTo = {};
    employe.message = '';
    employe.parameterTo = {};
    employe.state = '';
    // PARA UPDATE SE NECESITA EMPLOYEEPK SINO CREA OTRO COMO INSERT
    employe.employeeTo = {
      //documentNumber:'1',
      employeeStatus: 1,
      //name: 'Pamela Cristina',
      //lastName: 'Quispe Mendieta'
    };
    //this.tipeEmployee1.length  = 0
    this._employeeCtrl.getListEmployee(employe).subscribe(
      data => {
        // this.lstEmployee = data.lstEmployee;
        //console.log(this.lstEmployee);
        console.log(data);

        this.lstEmployeeTo = data.lstEmployeeTo;
        
        this.tipeEmployee1 = this.lstEmployeeTo;
        
        for (var i = 0; i < this.tipeEmployee1.length; i++) {
          for (var j = 0; j < this.tipeDocument.length; j++) {
            if (this.tipeDocument[j].parameterPk == this.tipeEmployee1[i].documentType) {
              this.tipeEmployee1[i].documentType = this.tipeDocument[j].name;
              console.log(this.tipeEmployee1[i].documentType);
              break;
            }
          }
        }
        for (var i = 0; i < this.tipeEmployee1.length; i++) {
          for (var j = 0; j < this.tipeEstado.length; j++) {
            if (this.tipeEstado[j].parameterPk == this.tipeEmployee1[i].state) {
              this.tipeEmployee1[i].state = this.tipeEstado[j].name;
              console.log(this.tipeEmployee1[i].state);
              break;
            }
          }
        }
        for (var i = 0; i < this.tipeEmployee1.length; i++) {
          for (var j = 0; j < this.tipeMarket.length; j++) {
            if (this.tipeMarket[j].parameterPk == this.tipeEmployee1[i].employeeGrade) {
              this.tipeEmployee1[i].employeeGrade = this.tipeMarket[j].name;
              console.log(this.tipeEmployee1[i].employeeGrade);
              break;
            }
          }
        }
        this.datos = new MatTableDataSource(this.tipeEmployee1);
        console.log(this.datos);
        this.datos.paginator = this.paginator;
        this.datos.sort = this.sort;
      },
      error => console.log(JSON.stringify(error))

    );

    const employe2: any = {};
    employe2.lstMaster = [null, null];
    employe2.lstParameter = [null, null];
    employe2.masterTo = {};
    employe2.message = '';
    employe2.parameterTo = {};
    employe2.state = '';
    // PARA UPDATE SE NECESITA EMPLOYEEPK SINO CREA OTRO COMO INSERT
    employe2.employeeTo = {
      //documentNumber:'1',
      employeeStatus: 1,
      //name: 'Pamela Cristina',
      //lastName: 'Quispe Mendieta'
    };

    this._employeeCtrl.getListEmployee(employe2).subscribe(
      data => {
        // this.lstEmployee = data.lstEmployee;
        //console.log(this.lstEmployee);
        console.log(data);
        this.lstEmployeeTo2 = data.lstEmployeeTo;
        // this.lstEmployeeTo2 = data.lstEmployeeTo;
        this.tipeEmployee3 = this.lstEmployeeTo2;
        console.log(this.tipeEmployee3);

      },
      error => console.log(JSON.stringify(error))

    );



  }

  nuevoColaborador() {
    this.router.navigate(['/apps/colaborador/nuevo'], {
      queryParams: { boton: 'Guardar' }
    });
  }

  buscarEmpleado() {

    this.applyFilter();
  }

  limpiarEmpleado() {
    this.buildForm();
  }

  modificarEmpleado() {
    //console.log(this.tipeEmployee1);
    //console.log(this.tipeEmployee);


    console.log("Entro a modificarEmpleado()");
    console.log(this.objectSelect[0]);
    this.router.navigate(['/apps/colaborador/nuevo'], {
      queryParams: {
        empleado: JSON.stringify(this.objectSelect[0]),
        boton: 'Actualizar'
      }
    });
    /*const tipoDocumento = this.formGroup.get('tipoDocumento').value;
    const documento = this.formGroup.get('documento').value;
    const estado = this.formGroup.get('estado').value;
    const nombres = this.formGroup.get('nombres').value;
    this.tipoDocumento = tipoDocumento;
    this.documento = documento;
    this.estado = estado;
    this.nombres = nombres;
    const employe: any = {};
        employe.lstMaster = [null,null];
        employe.lstParameter = [null,null];
        employe.masterTo = {};
        employe.message = '';
        employe.parameterTo = {};
        employe.state = '';
        // PARA UPDATE SE NECESITA EMPLOYEEPK SINO CREA OTRO COMO INSERT
        employe.employeeTo = {
            employeeStatus: 1,
            documentType:this.tipoDocumento,
            documentNumber:this.documento,
            state: this.estado,
            name: this.nombres,
     };
       this._employeeCtrl.updateEmployee(employe).subscribe(
        data => {
          console.log(data);
        },
        error => console.log(JSON.stringify(error))
      
    );*/
  }

  checkValue(e: any, row: EmployeeTo, i: number) {
    this.rowChecked = e.checked;
    this.valor(row, i);
    if (this.objectSelect.length < 2 && this.objectSelect.length > 0) {
      this.modHidden = false;
    } else {
      this.modHidden = true;
    }
  }

  checkValue1(e: any) {
    if (e.checked != false && (this.selection.select.length < 2 && this.selection.select.length > 0)) {
      this.modHidden = false;
    } else {
      this.modHidden = true;
    }
  }

  valor(row: EmployeeTo, i: number) {
    if ((row != null) && (i != null && i != -1) && (this.rowChecked != false)) {
      for (var i = 0; i < this.tipeEmployee3.length; i++) {
        if (row.employeePk == this.tipeEmployee3[i].employeePk) {
          this.objectSelect.push(this.tipeEmployee3[i]);
          break;
        }
      }


    } else {
      this.objectSelect.splice(i, 1);
    }
  }

  getFilterPredicate() {
    return (row: Empleado, filters: string) => {
      const filterArray = filters.split('$');
      const tipoDocumento = filterArray[0];
      const documento = filterArray[1];
      const estado = filterArray[2];
      const nombres = filterArray[3];
      const matchFilter = [];
      const columnTipoDocumento = row.tipoDocumento;
      const columnDocumento = row.numDocumento;
      const columnEstado = row.estado;
      const columnNombres = row.nombres;
      const customFilterETD = columnTipoDocumento.toLowerCase().includes(tipoDocumento);
      const customFilterED = columnDocumento.toLowerCase().includes(documento);
      const customFilterEE = columnEstado.toLowerCase().includes(estado);
      const customFilterEN = columnNombres.toLowerCase().includes(nombres);
      matchFilter.push(customFilterETD);
      matchFilter.push(customFilterED);
      matchFilter.push(customFilterEE);
      matchFilter.push(customFilterEN);
      return matchFilter.every(Boolean);
    };
  }

  applyFilter() {
    const tipoDocumento = this.formGroup.get('tipoDocumento').value;
    const documento = this.formGroup.get('documento').value;
    const estado = this.formGroup.get('estado').value;
    const nombres = this.formGroup.get('nombres').value;
    /*this.tipoDocumento = tipoDocumento;
    this.documento = documento;
    this.estado = estado;
    this.nombres = nombres;*/
    this.tipoDocumento = tipoDocumento === "" ? '' : tipoDocumento;
    this.documento = documento === "" ? '' : documento;
    this.estado = estado === "" ? '' : estado;
    this.nombres = nombres === "" ? '' : nombres;

    /*const filterValue = this.tipoDocumento + '$' + this.documento + '$' + this.estado + '$' + this.nombres;

    this.datos.filter = filterValue.trim().toLowerCase();
    console.log(this.datos.filter);*/
    console.log(this.tipoDocumento);
    console.log(this.estado);
    console.log(this.nombres);
    console.log(this.documento);
    this.filtroEmployee.numDoc = this.documento;
    this.filtroEmployee.nameEmplo = this.nombres;

   
    console.log(this.filtroEmployee);
    var employe: any = {};
    employe.lstMaster = [null, null];
    employe.lstParameter = [null, null];
    employe.masterTo = {};
    employe.message = '';
    employe.parameterTo = {};
    employe.state = '';
    // PARA UPDATE SE NECESITA EMPLOYEEPK SINO CREA OTRO COMO INSERT
    employe.employeeTo = {

      documentType: this.tipoDocumento,
      documentNumber: this.documento,
      state: this.estado,
      name: this.nombres,
      //lastName: 'Quispe Mendieta'
    };
    this._employeeCtrl.getListEmployee(employe).subscribe(
      data => {
        //this.employeeTo = data.employeeTo;
        //console.log(this.employeeTo);
        console.log(data);
        this.lstEmployeeTo = data.lstEmployeeTo;
        console.log(this.lstEmployeeTo);
        this.tipeEmployee1 = this.lstEmployeeTo;

        this.tipeEmployee2 = this.lstEmployeeTo;

        for (var i = 0; i < this.tipeEmployee1.length; i++) {
          for (var j = 0; j < this.tipeDocument.length; j++) {
            if (this.tipeDocument[j].parameterPk == this.tipeEmployee1[i].documentType) {
              this.tipeEmployee1[i].documentType = this.tipeDocument[j].name;
              console.log(this.tipeEmployee1[i].documentType);
              break;
            }
          }
        }

        for (var i = 0; i < this.tipeEmployee1.length; i++) {
          for (var j = 0; j < this.tipeEstado.length; j++) {
            if (this.tipeEstado[j].parameterPk == this.tipeEmployee1[i].state) {
              this.tipeEmployee1[i].state = this.tipeEstado[j].name;
              console.log(this.tipeEmployee1[i].state);
              break;
            }
          }
        }
        for (var i = 0; i < this.tipeEmployee1.length; i++) {
          for (var j = 0; j < this.tipeMarket.length; j++) {
            if (this.tipeMarket[j].parameterPk == this.tipeEmployee1[i].employeeGrade) {
              this.tipeEmployee1[i].employeeGrade = this.tipeMarket[j].name;
              console.log(this.tipeEmployee1[i].employeeGrade);
              break;
            }
          }
        }
        this.datos = new MatTableDataSource(this.tipeEmployee1);
        console.log(this.datos);
        this.datos.paginator = this.paginator;
        //this.datos.sort = this.sort;
      },
      error => console.log(JSON.stringify(error))
    );
    console.log(this.lstEmployeeTo);
  }

  buildForm() {
    const tipoDocumento = '';
    const documento = '';
    const estado = '';
    const nombres = '';
    this.formGroup = this.formBuilder.group({
      tipoDocumento: tipoDocumento,
      documento: documento,
      estado: estado,
      nombres: nombres
    });
  }

  ngOnDestroy(): void {
    //this.subscription.unsubscribe();
    /*this._unsubscribeAll.next();
    this._unsubscribeAll.complete();*/
    
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.datos.data.length;
    return numSelected === numRows;
  }

  deselectObject() {
    this.objectSelect = this.selection.selected;
    var object: any[] = [];
    this.objectSelect = object;
    this.selection.clear();
  }

  masterToggle() {
    this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: EmployeeTo): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.employeePk + 1}`;
  }
}

interface Archivos {
  documento: string;
  archivo: string;
}

interface Empleado {
  usuario: string;
  tipoDocumento: string;
  numDocumento: string;
  apePaterno: string;
  apeMaterno: string;
  nombres: string;
  fecNacimiento: string;
  corElectronicoPer: string;
  telefonoPer: string;
  corElectronicoEmp: string;
  telefonoEmp: string;
  direccion: string;
  estCivil: string;
  numHijos: number;
  genero: string;
  fecIngreso: string;
  fecTermino: string;
  puesto: string;
  horario: string;
  estado: string;
  lstArchivos: Array<Archivos>;
}
const ELEMENT_DATA: Empleado[] = [
  {
    usuario: 'ccclinsman', tipoDocumento: 'DNI', numDocumento: '71265819', apePaterno: 'Campos', apeMaterno: 'Cruz',
    nombres: 'Clinsman', fecNacimiento: '1996-04-21', corElectronicoPer: 'clinsmancc96@gmail.com',
    telefonoPer: '976378599', corElectronicoEmp: 'clinsmancc96@pandora-technologies.com', telefonoEmp: '992822123', direccion: 'Pz Las Azucenas Mz O6B Lt1',
    estCivil: 'Soltero', numHijos: 1, genero: 'Masculino', fecIngreso: '2020-02-01', fecTermino: '2020-12-01',
    puesto: 'Software Developer', horario: '8:00AM - 5:00PM', estado: 'Activo', lstArchivos: [{ documento: 'DNI', archivo: 'DNI_46360431.JPG' }]
  },
  {
    usuario: 'cnjuan', tipoDocumento: 'Pasaporte', numDocumento: '32432434', apePaterno: 'Cruz', apeMaterno: 'Nomberto',
    nombres: 'Juan', fecNacimiento: '1994-06-14', corElectronicoPer: 'jcnomberto@gmail.com',
    telefonoPer: '993320434', corElectronicoEmp: 'jcnomberto@pandora-technologies.com', telefonoEmp: '949222129', direccion: 'Pz La Loma Azul Mz O6C Lt 3',
    estCivil: 'Soltero', numHijos: 3, genero: 'Masculino', fecIngreso: '2020-03-11', fecTermino: '2020-05-21',
    puesto: 'Software Developer', horario: '8:00AM - 5:00PM', estado: 'Inactivo', lstArchivos: []
  },
  {
    usuario: 'yralberto', tipoDocumento: 'Carnet', numDocumento: '65433423', apePaterno: 'Yamil', apeMaterno: 'Rios',
    nombres: 'Alberto', fecNacimiento: '1994-12-03', corElectronicoPer: 'jralberto@gmail.com',
    telefonoPer: '996664443', corElectronicoEmp: 'jralberto@pandora-technologies.com', telefonoEmp: '992382838', direccion: 'Pz Las Palmeras Mz O5BN Lt 9',
    estCivil: 'Soltero', numHijos: 6, genero: 'Masculino', fecIngreso: '2020-01-01', fecTermino: '2020-07-12',
    puesto: 'Software Developer', horario: '8:00AM - 5:00PM', estado: 'Activo', lstArchivos: []
  },
  {
    usuario: 'sgkihara', tipoDocumento: 'DNI', numDocumento: '65424323', apePaterno: 'Sanches', apeMaterno: 'Garate',
    nombres: 'Kihara', fecNacimiento: '1994-09-08', corElectronicoPer: 'ksgarate@gmail.com',
    telefonoPer: '995433333', corElectronicoEmp: 'ksgarate@pandora-technologies.com', telefonoEmp: '928391282', direccion: 'Pz Las Gardenias Mz O7C Lt 12',
    estCivil: 'Soltero', numHijos: 2, genero: 'Femenino', fecIngreso: '2020-03-03', fecTermino: '2020-12-01',
    puesto: 'Software Developer', horario: '8:00AM - 5:00PM', estado: 'Activo', lstArchivos: []
  }
];