import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card'
import { MatSelectModule } from '@angular/material/select'
import { FuseSharedModule } from '@fuse/shared.module';

import { Register2Component } from 'app/main/pages/authentication/register-2/register-2.component';
import { FuseWidgetModule } from '@fuse/components';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTableModule } from '@angular/material/table';
import {MatRadioModule} from '@angular/material/radio';
import { SearchVacationComponent } from './searchVacation.component';
import { MatAutocompleteModule, MatPaginatorModule } from '@angular/material';
import { DatePipe } from '@angular/common';

const routes = [
    {
        path     : '**',
        component: SearchVacationComponent
    }
];

@NgModule({
    declarations: [
        SearchVacationComponent,
        
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatCardModule,
        MatSelectModule,
        FuseSharedModule,
        MatButtonModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatTabsModule,
        MatRadioModule,
        MatTableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),
        ChartsModule,
        NgxChartsModule,
        MatPaginatorModule,
        FuseSharedModule,
        FuseWidgetModule,
        MatAutocompleteModule,
        
    ],
    providers:[DatePipe]

    
})
export class SearchVacationModule
{
}
