import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Observable } from 'rxjs/internal/Observable';
import { VacationController } from 'app/common/rest/services/VacationController';
import { DatePipe } from '@angular/common';

interface registerVacation {
    id:string;
    employe:string;
    fechaInicial:Date;
    fechaFinal:Date;
    cantDias:string;
    estado:string;
  }
  const ELEMENT_DATA: registerVacation[] = [
    {id:'2' ,employe:'ingeniero' , fechaInicial: new Date('10/27/2020'), fechaFinal: new Date('10/27/2020'), cantDias: '2',estado:'confirmado'},
    {id:'1' ,employe:'ingeniero' ,fechaInicial: new Date('10/27/2020'), fechaFinal: new Date('10/27/2020'), cantDias: '2',estado:'confirmado'},
    {id:'3' , employe:'ingeniero',fechaInicial: new Date('10/27/2020'), fechaFinal: new Date('10/27/2020'), cantDias: '2',estado:'confirmado'},
    {id:'4' ,employe:'ingeniero' ,fechaInicial: new Date('10/27/2020'), fechaFinal: new Date('10/27/2020'), cantDias: '2',estado:'confirmado'},
    {id:'4' ,employe:'ingeniero' ,fechaInicial: new Date('10/27/2020'), fechaFinal: new Date('10/27/2020'), cantDias: '2',estado:'confirmado'},
    {id:'4' ,employe:'ingeniero' ,fechaInicial: new Date('10/27/2020'), fechaFinal: new Date('10/27/2020'), cantDias: '2',estado:'confirmado'},
    
  ];
@Component({
    selector     : 'searchVacation',
    templateUrl  : './searchVacation.component.html',
    styleUrls    : ['./searchVacation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SearchVacationComponent implements OnInit, OnDestroy
{
    objectSelect:any[]=[];
  
    formGroup: FormGroup;
    columnas: string[] = ['id','employe','fechaInicial', 'fechaFinal', 'cantDias','estado', 'actions'];
    datos: MatTableDataSource<registerVacation>
    startVacation: Date;
    endVacation: Date;
    dayRequest: number;
    lstVacation: any = [{}];
    selection = new SelectionModel<registerVacation>(true, []);
    get diaFechaInicial() { return this.formGroup.get('diaFechaInicial').value; }
    get diaFechaFinal() { return this.formGroup.get('diaFechaFinal').value; }
    lstVacationState: any = [{}];
    pipe: DatePipe;
    myControl = new FormControl();
    options: string[] = [];
    options2: string[] = ['clinsman', 'yamil', 'juan', 'Clinsman Campos', 'Juan Balboa', 'Cris Martinez', 'Clinsman Campos Cruz', 'Juan Cruz Nomberto', 'Reynaldo', 'Silvia Jurado'];
    filteredOptions: Observable<string[]>;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    constructor(
        private _formBuilder: FormBuilder,
        private _lstVacation: VacationController,
        private datePipe: DatePipe
    )
    {
      //this.pipe = new DatePipe('en');
      //const dato = ELEMENT_DATA;
      this.datos = new MatTableDataSource();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.datos.data.length;
      return numSelected === numRows;
    }
    deselectObject(){
     this.objectSelect=this.selection.selected;
     var object: any[] = [];
     this.objectSelect= object;
     this.selection.clear();
    }  
    masterToggle() {
      this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
     }
    checkboxLabel(row?: registerVacation): string {
      if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
     }
     return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }
    /**
     * On init
     */
    /*formGroup = new FormGroup({
      diaFechaInicial:  new FormControl(),
      diaFechaFinal:  new FormControl(),
    });*/
    ngOnInit(): void
    {
      let stateVacation = JSON.parse(localStorage.getItem('currentStateVacation'));
      console.log(stateVacation);
      this.lstVacationState = stateVacation;
      this.options = this.options2;
      this.datos.paginator = this.paginator;
      this.datos.sort = this.sort;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
      this.buildForm();
      const vacation: any = {};
          // PARA UPDATE SE NECESITA VacationPk SINO CREA OTRO COMO INSERT
       vacation.vacationTo = {
        //employeeFk: 2,
        //vacationPk:124,
        //startVacation: '2010-02-05',
        //endVacation: '2011-04-05'
        //name:'juancho96',
        //dayRequest:1, 
       };
       this._lstVacation.getListVacation(vacation).subscribe(
        data => {
          this.lstVacation = data.lstVacation;
          //var date = new Date();
          for (let i = 0; i < this.lstVacation.length; i++) {
            //var date = this.lstVacation[i].startVacation;
            //const element = this.lstVacation[i];
            //element.startVacation = new Date(element.startVacation);
           
            
            var date =this.datePipe.transform(this.lstVacation[i].startVacation,"dd/MM/yyyy");
            var date2 =this.datePipe.transform(this.lstVacation[i].endVacation,"dd/MM/yyyy");
            this.lstVacation[i].startVacation = date;
            this.lstVacation[i].endVacation = date2;
            //var resultado = this.lstVacation[2].startVacation.replace('-','/');
            //console.log(resultado);
          }
          //const element = this.lstVacation;
          //this.datePipe.transform(this.lstVacation.startVacation,"dd/MM/yyyy");
         // const element = this.lstVacation;
          //element.startVacation =new Date(element.startVacation);
          console.log(data);
          this.datos = new MatTableDataSource(this.lstVacation);
          console.log(this.datos);
          this.datos.paginator = this.paginator;
          this.datos.sort = this.sort;
      },
      error => console.log(JSON.stringify(error))
       );
    }
    private _filter(value: string): string[] {
      const filterValue = value.toLowerCase();
  
      return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }
   
    private buildForm() {
      
      const name = "";
      const startVacation = "";
      const endVacation = "";
      const dayRequest = "";
      const employeeFk ="";
      this.formGroup = this._formBuilder.group({
        name: name,
        startVacation: startVacation,
        endVacation: endVacation,
        dayRequest: dayRequest,
        employeeFk: employeeFk,
      });
  }
  
    ngOnDestroy(): void
    {

    }

    checkValue(){

    }

    checkValue1(){
      
    }
}
