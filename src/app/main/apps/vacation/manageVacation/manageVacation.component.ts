import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/internal/Observable';
import { VacationController } from 'app/common/rest/services/VacationController';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { EmployeeTo } from 'app/common/rest/services/Beans';
import { SelectionModel } from '@angular/cdk/collections';

interface registerVacation {
    fechaInicial:string;
    fechaFinal:string;
    cantDias:string;
  }
  const ELEMENT_DATA: registerVacation[] = [
    {fechaInicial:'12/04/2020' , fechaFinal: 'Desarrollador', cantDias: '2'},
    {fechaInicial: '12/04/2020', fechaFinal: 'Desarrollador', cantDias: '2'},
    {fechaInicial: '12/04/2020', fechaFinal: 'Desarroladdor', cantDias: '2'},
    {fechaInicial: '12/04/2020', fechaFinal: 'Desarrollador', cantDias: '2'},
    
  ];
@Component({
    selector     : 'manageVacation',
    templateUrl  : './manageVacation.component.html',
    styleUrls    : ['./manageVacation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ManageVacationComponent implements OnInit, OnDestroy
{
    formGroup: FormGroup;
    objectSelect:any[]=[];
    columnas: string[] = ['fechaInicial', 'fechaFinal', 'cantDias', 'actions'];
    datos: any = [{}];
    //datos: MatTableDataSource<registerVacation>;
    //selection = new SelectionModel<registerVacation>(true, []);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    
    dateInicial:Date;
    lstVacation: any = [{}];
    name: string = "";
    startVacation: Date;
    endVacation: Date;
    dayRequest: number;
    lstEmployeeTo: EmployeeTo;
    lstEmployee: any = [{}];
    //lstEmployeeName: any = [{}];
    myControl = new FormControl();
    options: string[] = [];
    lstEmployeeName: string[] = [];
    lstEmployeeNameVar: string[] = [];
    filteredOptions: Observable<string[]>;
    constructor(
        private _lstVacation: VacationController,
        private formBuilder: FormBuilder,
        private _employeeCtrl: EmployeeController,

    )
    {
      //const dato = ELEMENT_DATA;
      this.datos = new MatTableDataSource();
    }

   
    ngOnInit(): void
    {
      console.log(this.options);
      let employe: any = {};
    
      //var employe= new employe();
      employe.lstMaster = [null, null];
      employe.lstParameter = [null, null];
      employe.masterTo = {};
      employe.message = '';
      employe.parameterTo = {};
      employe.state = '';
      employe.employeeTo = {
        employeeStatus: 1,
       
      };
      this._employeeCtrl.getListEmployee(employe).subscribe(
        data => {
          console.log(data);
          this.lstEmployeeTo = data.lstEmployeeTo;
          this.lstEmployee = this.lstEmployeeTo;
          console.log(this.lstEmployee);
          //this.lstEmployeeTo.name;
          //console.log(this.lstEmployee.name);
          for (var i = 0; i < this.lstEmployee.length; i++) {
            this.lstEmployeeName.push(this.lstEmployee[i].name);
            console.log(this.lstEmployee[i].name);
          }
          this.lstEmployeeNameVar = Array.from(new Set(this.lstEmployeeName));
          
          this.options = this.lstEmployeeNameVar;
          console.log(this.options);
          this.filteredOptions = this.formGroup.controls.name.valueChanges
          .pipe(
          startWith(''),
          map(value => this._filter(value))
         );
          
        },
        error => console.log(JSON.stringify(error))
      )
    //if(this.options[0].length){
     
    
      
     this.buildForm();
     const vacation: any = {};
          // PARA UPDATE SE NECESITA VacationPk SINO CREA OTRO COMO INSERT
       vacation.vacationTo = {
        //employeeFk: 2,
        //vacationPk:124,
        //startVacation: '2010-02-05',
        //endVacation: '2011-04-05'
        //name:'juancho96',
        //dayRequest:1, 
       };
       this._lstVacation.getListVacation(vacation).subscribe(
        data => {
          this.lstVacation = data.lstVacation;
          console.log(data);
          this.datos = new MatTableDataSource(this.lstVacation);
          console.log(this.datos);
          this.datos.paginator = this.paginator;
          this.datos.sort = this.sort;
      },
      error => console.log(JSON.stringify(error))
       );
       /*this._lstVacation.createVacation(vacation).subscribe(
            data => {
                //this.lstVacation = data.lstVacation;
                console.log(data);
            },
            error => console.log(JSON.stringify(error))
       );*/

      /*this._lstVacation.updateVacation(vacation).subscribe(
        data => {
            //this.lstVacation = data.lstVacation;
            console.log(data);
            //console.log(this.lstVacation);
        }
         );*/
    }
    private _filter(value: string): string[] {
      const filterValue = value.toLowerCase();
  
      return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }

    private buildForm() {
      const name = "";
      const startVacation = "";
      const endVacation = "";
      const dayRequest = "";
      this.formGroup = this.formBuilder.group({
        name: name,
        startVacation: startVacation,
        endVacation: endVacation,
        dayRequest: dayRequest,
 
      });
  }

    onCreateVacation() {
      const name = this.formGroup.get('name').value;
      const startVacation = this.formGroup.get('startVacation').value;
      const endVacation = this.formGroup.get('endVacation').value;
      const dayRequest = this.formGroup.get('dayRequest').value;
      this.name = name;
      this.startVacation = startVacation;
      this.endVacation = endVacation;
      this.dayRequest = dayRequest;
      console.log(this.name);
      console.log(this.startVacation);
      console.log(this.dayRequest);

      const vacation: any = {};
      vacation.vacationTo = {
        employeeFk: 2,
        startVacation: this.startVacation,
        endVacation: this.endVacation,
        name: this.name,
        dayRequest:this.dayRequest, 
       };
       this._lstVacation.createVacation(vacation).subscribe(
        data => {
            //this.lstVacation = data.lstVacation;
            console.log(data);
        },
        error => console.log(JSON.stringify(error))
   );
    }

    
    ngOnDestroy(): void
    {

    }
}
