import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class BuscarComponent implements OnInit {

  colaborador: string = "";
  estado: string = "";
  fecha: string = "";
  hora: number = 0;

  lsthe: HorasExtras[] = [];

  estados = [
    { estado: 'Pendiente' },
    { estado: 'Confirmado' },
    { estado: 'Rechazado' }
  ];

  myControl = new FormControl();
  options: Colaborador[] = [
    { name: 'Clinsman Campos Cruz' },
    { name: 'Alberto Yamil Rios' },
    { name: 'Juan Cruz Nomberto' },
    { name: 'Kihara Sanches Garate' }
  ];
  filteredOptions: Observable<Colaborador[]>;

  public formGroup: FormGroup;
  columnas: string[] = ['id', 'nombre', 'fecha', 'horaInicial', 'horaFinal', 'estado', 'actions'];
  objectSelect: any[] = [];
  rowChecked: boolean = false;
  modHidden: boolean = true;

  datos: MatTableDataSource<HorasExtras>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<HorasExtras>(true, []);

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private rutaActiva: ActivatedRoute,
  ) {
    const dato = ELEMENT_DATA;
    this.datos = new MatTableDataSource(dato);
    this.rutaActiva.queryParams.subscribe(params => {
      this.lsthe = JSON.parse(params['lsthe']);
    });
    if(this.lsthe != null && this.lsthe != []){
      this.lsthe.forEach(element => {
        this.datos.data.push(element);
      });
    }
  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.options.slice())
      );
    this.buildForm();
    this.datos.paginator = this.paginator;
    this.datos.sort = this.sort;
    this.datos.filterPredicate = this.getFilterPredicate();
  }

  displayFn(user: Colaborador): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): Colaborador[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  registrarHorasExtras() {
    this.router.navigate(['/apps/horasExtras/registrar']);
  }

  private buildForm() {
    const colaborador = "";
    const estado = "";
    const fecha = "";
    const hora = "";
    this.formGroup = this.formBuilder.group({
      colaborador: colaborador,
      estado: estado,
      fecha: fecha,
      hora: hora
    });
  }

  OnColaboradorSelected(colaborador): string {
    return colaborador.nombresYapellidos;

  }

  buscarHorasExtras() {
    this.applyFilter();
  }

  limpiarHorasExtras() {
    this.buildForm();
  }

  opcion(i: number) {
    switch (i) {
      case 1: {
        for (let i = 0; i < this.objectSelect.length; i++) {
          this.objectSelect[i].estado = "Confirmado";
        }
        break;
      }
      case 2: {
        for (let i = 0; i < this.objectSelect.length; i++) {
          this.objectSelect[i].estado = "Rechazado";
        }
        break;
      }
      case 3: {
        if (this.objectSelect.length < 2) {
          this.router.navigate(['/apps/horasExtras/registrar']);
        }
        break;
      }
      default: {
        break;
      }
    }
  }

  checkValue(e: any, row: HorasExtras, i: number) {
    this.rowChecked = e.checked;
    this.valor(row, i);
    if (this.objectSelect.length < 2 && this.objectSelect.length > 0) {
      this.modHidden = false;
    } else {
      this.modHidden = true;
    }
  }

  checkValue1(e: any) {
    if (e.checked != false && (this.selection.select.length < 2 && this.selection.select.length > 0)) {
      this.modHidden = false;
    } else {
      this.modHidden = true;
    }
  }

  valor(row: HorasExtras, i: number) {
    if ((row != null) && (i != null && i != -1) && (this.rowChecked != false)) {
      this.objectSelect.push(row);
    } else {
      this.objectSelect.splice(i, 1);
    }
  }

  getFilterPredicate() {
    return (row: HorasExtras, filters: string) => {
      const filterArray = filters.split('$');
      const colaborador = filterArray[0];
      const estado = filterArray[1];
      const fecha = filterArray[2];
      const matchFilter = [];
      const columnColaborador = row.nombre + ' ' + row.apePaterno + ' ' + row.apeMaterno;
      const columnEstado = row.estado;
      const columnFecha = row.fecha;
      const customFilterNC = columnColaborador.toLowerCase().includes(colaborador);
      const customFilterHE = columnEstado.toLowerCase().includes(estado);
      const customFilterHF = columnFecha.toLowerCase().includes(fecha);
      matchFilter.push(customFilterNC);
      matchFilter.push(customFilterHE);
      matchFilter.push(customFilterHF);
      return matchFilter.every(Boolean);
    };
  }

  applyFilter() {
    const colaborador = this.formGroup.get('colaborador').value;
    const estado = this.formGroup.get('estado').value;
    const fecha = this.formGroup.get('fecha').value;
    //const fecha = this.datePipe.transform(this.fecha, 'dd-MM-yyyy');
    //const fecha = this.datePipe.transform(this.fecha, 'dd-MM-yyyy');
    //const fecha = formatDate(colaborador, "dd-MM-yyyy", "en-ES");

    this.colaborador = colaborador === "" ? '' : colaborador;
    this.estado = estado === "" ? '' : estado;
    this.fecha = fecha === "" ? '' : fecha;

    const filterValue = this.colaborador + '$' + this.estado + '$' + (this.datePipe.transform(this.fecha, 'dd-MM-yyyy') === null ? '' : this.datePipe.transform(this.fecha, 'dd-MM-yyyy'));

    this.datos.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy(): void {

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.datos.data.length;
    return numSelected === numRows;
  }

  deselectObject() {
    this.objectSelect = this.selection.selected;
    var object: any[] = [];
    this.objectSelect = object;
    this.selection.clear();
  }

  masterToggle() {
    this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: HorasExtras): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

}

export interface Colaborador {
  name: string;
}

interface HorasExtras {
  id: string;
  nombre: string;
  apePaterno: string;
  apeMaterno: string;
  fecha: string;
  consumo: string;
  motivo: string;
  horaInicial: string;
  horaFinal: string;
  horaTotal: string;
  estado: string;
}

const ELEMENT_DATA: HorasExtras[] = [
  {
    id: '1', nombre: 'Clinsman', apePaterno: 'Campos', apeMaterno: 'Cruz', fecha: '01-01-2018',
    consumo: 'Egreso', motivo: 'Salud', horaInicial: '19:00 PM', horaFinal: '20:30 PM', horaTotal: '02:30 h',
    estado: 'Pendiente'
  },
  {
    id: '2', nombre: 'Alberto', apePaterno: 'Yamil', apeMaterno: 'Rios', fecha: '03-04-2019',
    consumo: 'Egreso', motivo: 'Deluto', horaInicial: '18:00 PM', horaFinal: '21:30 PM', horaTotal: '03:30 h',
    estado: 'Confirmado'
  },
  {
    id: '3', nombre: 'Juan', apePaterno: 'Cruz', apeMaterno: 'Nomberto', fecha: '21-06-2020',
    consumo: 'Egreso', motivo: 'Cumpleaños', horaInicial: '19:30 PM', horaFinal: '22:30 PM', horaTotal: '03:00 h',
    estado: 'Rechazado'
  }
];