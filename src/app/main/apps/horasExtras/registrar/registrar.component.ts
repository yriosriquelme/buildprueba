import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class RegistrarComponent implements OnInit {
  id: string = "";
  colaborador: string = "";
  fecha: string = "";
  consumo: string = "";
  horaInicio: string = "00:00";
  horaFin: string = "00:00";
  motivo: string = "";

  colaboradores = [
    { colaborador: 'Clinsman Campos Cruz' },
    { colaborador: 'Alberto Yamil Rios' },
    { colaborador: 'Juan Cruz Nomberto' },
    { colaborador: 'Kihara Sanches Garate' }
  ];

  consumos = [
    { consumo: 'Ingreso' },
    { consumo: 'Egreso' }
  ];

  horaExtra: HorasExtras = null;
  lstHorasExtras: Array<HorasExtras> = null;

  public formGroup: FormGroup;
  columnas: string[] = ['actions', 'motivo', 'fecha', 'inicio', 'fin', 'total'];
  objectSelect: any[] = [];

  datos: MatTableDataSource<HorasExtras>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<HorasExtras>(true, []);
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private datePipe: DatePipe
  ) {
    const dato = ELEMENT_DATA;
    this.datos = new MatTableDataSource(dato);
  }

  ngOnInit() {
    this.buildForm();
    this.datos.paginator = this.paginator;
    this.datos.sort = this.sort;
    this.datos.data = [];
  }

  regresarBuscarHorasExtras() {
    this.router.navigate(['/apps/horasExtras/buscar']);
  }

  private buildForm() {
    const colaborador = "";
    const fecha = "";
    const consumo = "";
    const horaInicio = "00:00";
    const horaFin = "00:00";
    const motivo = "";
    this.formGroup = this.formBuilder.group({
      colaborador: colaborador,
      fecha: fecha,
      consumo: consumo,
      horaInicio: horaInicio,
      horaFin: horaFin,
      motivo: motivo
    });
  }

  agregarHorasExtras() {
    switch (this.colaborador) {
      case 'Clinsman Campos Cruz':
        this.id = '1';
        break;
      case 'Alberto Yamil Rios':
        this.id = '2';
        break;
      case 'Juan Cruz Nomberto':
        this.id = '3';
        break;
      case 'Kihara Sanches Garate':
        this.id = '4';
        break;
      default:
        this.id = '0';
        break;
    }
    if (this.id != '0' && this.id != null) {
      var str = this.colaborador;
      var colaborador = str.split(" ", 3);
      var inicio = this.horaInicio;
      var fin = this.horaFin;
      var hora1Split = inicio.split(":", 2);
      var inicioHoras = Number(hora1Split[0]);
      var inicioMinutos = Number(hora1Split[1]);
      var hora2Split = fin.split(":", 2);
      var finHoras = Number(hora2Split[0]);
      var finMinutos = Number(hora2Split[1]);
      var circunstancia1 = 'AM';
      var circunstancia2 = 'AM';
      if(inicioHoras >= 12){
        circunstancia1 = 'PM';
      }
      if(finHoras >= 12){
        circunstancia2 = 'PM';
      }
      if (finHoras >= inicioHoras) {
        if (finMinutos >= inicioMinutos) {
          var transcurridoMinutos = finMinutos - inicioMinutos;
          var transcurridoHoras = finHoras - inicioHoras;
          if (transcurridoMinutos < 0) {
            transcurridoHoras--;
            transcurridoMinutos = 60 + transcurridoMinutos;
          }
          var horas = transcurridoHoras.toString();
          var minutos = transcurridoMinutos.toString();
          if (horas.length < 2) {
            horas = '0' + horas;
          }
          if (minutos.length < 2) {
            minutos = '0' + minutos;
          }
          var total = horas + ':' + minutos;
          this.horaExtra = {
            id: this.id, nombre: colaborador[0], apePaterno: colaborador[1], apeMaterno: colaborador[2],
            fecha: this.datePipe.transform(this.fecha, 'dd-MM-yyyy'), consumo: this.consumo, motivo: this.motivo,
            horaInicial: this.horaInicio + ' ' + circunstancia1, horaFinal: this.horaFin + ' ' + circunstancia2, 
            horaTotal: total + ' h', estado: 'Pendiente'
          };
          this.datos.data.push(this.horaExtra);
          this.datos._updateChangeSubscription();
          this.buildForm();
        }
      }
    }
  }

  registrarHorasExtras() {
    if(this.datos.data != null && this.datos.data != []){
      this.router.navigate(['/apps/horasExtras/buscar'], {
        queryParams: {
          lsthe : JSON.stringify(this.datos.data)
        }
      });
    }else{
      this.router.navigate(['/apps/horasExtras/buscar']);
    }
  }

  limpiarHorasExtras() {
    this.buildForm();
  }

  ngOnDestroy(): void {

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.datos.data.length;
    return numSelected === numRows;
  }

  deselectObject() {
    this.objectSelect = this.selection.selected;
    var object: any[] = [];
    this.objectSelect = object;
    this.selection.clear();
  }

  masterToggle() {
    this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: HorasExtras): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
}

interface HorasExtras {
  id: string;
  nombre: string;
  apePaterno: string;
  apeMaterno: string;
  fecha: string;
  consumo: string;
  motivo: string;
  horaInicial: string;
  horaFinal: string;
  horaTotal: string;
  estado: string;
}

const ELEMENT_DATA: HorasExtras[] = [
  /*{
    id: '1', nombre: 'Clinsman', apePaterno: 'Campos', apeMaterno: 'Cruz', fecha: '01-01-2018',
    consumo: 'Ingreso', motivo: 'Lunes - Salud', horaInicial: '09:00 AM', horaFinal: '10:00 AM',
    horaTotal: '01:00 h', estado: 'Pendiente'
  },
  {
    id: '2', nombre: 'Alberto', apePaterno: 'Yamil', apeMaterno: 'Rios', fecha: '03-04-2019',
    consumo: 'Ingreso', motivo: 'Viernes - Deluto', horaInicial: '16:00 PM', horaFinal: '17:30 PM',
    horaTotal: '01:30 h', estado: 'Pendiente'
  },
  {
    id: '3', nombre: 'Juan', apePaterno: 'Cruz', apeMaterno: 'Nomberto', fecha: '21-06-2020',
    consumo: 'Ingreso', motivo: 'Jueves - Estudio', horaInicial: '17:30 PM', horaFinal: '18:40 PM',
    horaTotal: '02:10 h', estado: 'Pendiente'
  }*/
];