import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'colaborador/buscar',
        loadChildren: './colaborador/buscar/buscar.module#SearchEmployeModule'
    },
    {
        path        : 'colaborador/nuevo',
        loadChildren: './colaborador/nuevo/nuevo.module#NewEmployeModule'
    },
    {
        path        : 'horasExtras/buscar',
        loadChildren: './horasExtras/buscar/buscar.module#BuscarModule'
    },
    {
        path        : 'horasExtras/registrar',
        loadChildren: './horasExtras/registrar/registrar.module#RegistrarModule'
    },
    {
        path        : 'assistance/manageAssistance',
        loadChildren: './assistance/manageAssistance/manage.module#ManageAssistanceModule'
    },
    {
        path        : 'assistance/searchAssistance',
        loadChildren: './assistance/searchAssistance/search.module#SearchAssistanceModule'
    },
    {
        path        : 'requerimientos/buscar',
        loadChildren: './requerimientos/buscar/searchRequest.module#SearchRequestModule'
    },
    {
        path        : 'requerimientos/regis-masivo',
        loadChildren: './requerimientos/regis-masivo/regis-masivo.module#RegisMasivoModule'
    },
    {
        path        : 'requerimientos/registrar',
        loadChildren: './requerimientos/registrar/registry.module#RegistryRequestModule'
    },
    {
        path        : 'requerimientos/reporte',
        loadChildren: './requerimientos/reporte/reporte.module#ReporteModule'
    },
    {
        path        : 'permits/managePermits',
        loadChildren: './permits/managePermits/managePermits.module#ManagePermitsModule'
    },
    {
        path        : 'permits/searchPermits',
        loadChildren: './permits/searchPermits/searchPermits.module#SearchPermitsModule'
    },
    {
        path        : 'dashboards/analytics',
        loadChildren: './dashboards/analytics/analytics.module#AnalyticsDashboardModule'
    },
    {
        path        : 'dashboards/project',
        loadChildren: './dashboards/project/project.module#ProjectDashboardModule'
    },
    {
        path        : 'mail',
        loadChildren: './mail/mail.module#MailModule'
    },
    {
        path        : 'mail-ngrx',
        loadChildren: './mail-ngrx/mail.module#MailNgrxModule'
    },
    {
        path        : 'chat',
        loadChildren: './chat/chat.module#ChatModule'
    },
    {
        path        : 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    },
    {
        path        : 'e-commerce',
        loadChildren: './e-commerce/e-commerce.module#EcommerceModule'
    },
    {
        path        : 'academy',
        loadChildren: './academy/academy.module#AcademyModule'
    },
    {
        path        : 'todo',
        loadChildren: './todo/todo.module#TodoModule'
    },
    {
        path        : 'file-manager',
        loadChildren: './file-manager/file-manager.module#FileManagerModule'
    },
    {
        path        : 'contacts',
        loadChildren: './contacts/contacts.module#ContactsModule'
    },
    {
        path        :'report/assistanceReport',
        loadChildren: './report/assistanceReport/assistanceReport.module#AssistanceReportModule'
    },
    {
        path        :'report/reports',
        loadChildren: './report/reports/reports.module#ReportsModule'
    },
    {
        path        : 'scrumboard',
        loadChildren: './scrumboard/scrumboard.module#ScrumboardModule'
    },
    {
        path        :'vacation/manageVacation',
        loadChildren: './vacation/manageVacation/manageVacation.module#ManageVacationModule'
    },
    {
        path        :'vacation/searchVacation',
        loadChildren: './vacation/searchVacation/searchVacation.module#SearchVacationModule'
    },
    {
        path        :'vacation/controlVacation',
        loadChildren: './vacation/controlVacation/controlVacation.module#ControlVacationModule'
    },
   
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule
    ],
    declarations: []
})
export class AppsModule
{
}