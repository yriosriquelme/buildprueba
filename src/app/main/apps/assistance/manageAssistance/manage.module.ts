import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { ManageAssistanceComponent } from './manage.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';

import { MatCardModule } from '@angular/material/card';

import { MatTableModule } from '@angular/material/table';
import { AdminManageAssistanceComponent } from './admin-manage-assistance/admin-manage-assistance.component';

const routes: Routes = [
    {
        path     : 'manageAssistanceComponent',
        component: ManageAssistanceComponent
    },
    {
        path     : 'adminManageAssistanceComponent',
        component: AdminManageAssistanceComponent
    }
];

@NgModule({
    declarations: [
        ManageAssistanceComponent,
        AdminManageAssistanceComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatTabsModule,
        MatCardModule,

        MatTableModule,

        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),
        ChartsModule,
        NgxChartsModule,

        FuseSharedModule,
        FuseWidgetModule
    ]
})
export class ManageAssistanceModule
{
}

