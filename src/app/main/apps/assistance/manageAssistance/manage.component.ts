import { Component, ViewEncapsulation, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import {take} from 'rxjs/operators';
import { MatTable } from '@angular/material';

export interface PeriodicElement {
    name: string;
    position: string;
    weight: string;
    symbol: string;
    coment: string;
  }
const ELEMENT_DATA: PeriodicElement[] = [
    {position: '2020-07-10', name: '9am', weight: '6pm', symbol: '8', coment: 'Puede mejorar.'},
    {position: '2020-07-10', name: '9am', weight: '5pm', symbol: '7', coment: 'En proceso.'},
    /*{position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
    {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
    {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
    {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
    {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
    {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
    {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},*/
];

@Component({
    selector: 'manage-assistance',
    templateUrl: './manage.component.html',
    styleUrls: ['./manage.component.scss'],
    encapsulation: ViewEncapsulation.None
})
  
  

export class ManageAssistanceComponent {
    displayedColumns = ['position', 'name', 'weight', 'symbol', 'coment'];
    dataSource = ELEMENT_DATA;
    registroMultiple = false;
    infoEntrada: any = new Date();
    infoSalida: any = new Date();
    horasTrabajadasFinal: any;
    diffMs: any;
    diffHrs: any;
    diffMins: any;


    public formGroup: FormGroup;

    constructor(
        private _ngZone: NgZone,
        private formBuilder: FormBuilder) {

    }

    @ViewChild('autosize', {static: false}) autosize: CdkTextareaAutosize;

    @ViewChild(MatTable, {static: false}) table: MatTable<any>;

    triggerResize() {
        // Wait for changes to be applied, then trigger textarea resize.
        this._ngZone.onStable.pipe(take(1))
            .subscribe(() => this.autosize.resizeToFitContent(true));
      }

    /**
     * On init
     */
    ngOnInit(): void {
        this.infoEntrada = new Date();
        this.buildForm();
    }
    private buildForm() {
        const dateLength = 10;
        const today = new Date().toISOString().substring(0, dateLength);
        const empleado = '1';
        const horaEntrada = this.infoEntrada.getHours() + ':' + this.infoEntrada.getMinutes();
        const horaEntradaDia = 'am';
        const horaSalida = this.infoSalida.getHours() + ':' + this.infoSalida.getMinutes();
        const horaSalidaDia = 'pm';
        const horasTrabajadas = this.horasTrabajadasFinal;
        const comentario = 'Asumio todas sus actividades con total normalidad.';
        /*const name = 'CLINSMAN CAMPOS';*/
        this.formGroup = this.formBuilder.group({
            /*empleado: name.toLowerCase()*/
            empleado: empleado,
            diaAsistencia: today,
            horaEntrada: horaEntrada,
            horaEntradaDia: horaEntradaDia,
            horaSalida: horaSalida,
            horaSalidaDia: horaSalidaDia,
            horasTrabajadas: horasTrabajadas,
            comentario: comentario
        });
    }

    guardar(): void{
        if (this.formGroup.value.horaEntrada != '') {
            this.infoSalida = new Date();
            this.diffMs = (this.infoSalida - this.infoEntrada); // milliseconds between now & Christmas
            this.diffHrs = Math.floor((this.diffMs % 86400000) / 3600000); // hours
            this.diffMins = Math.round(((this.diffMs % 86400000) % 3600000) / 60000); // minutes
            this.horasTrabajadasFinal = this.diffHrs + ':' + this.diffMins;
            this.buildForm();
        }
        
    }
    
    onSubmit(): void {
        console.log(this.formGroup.value.comentario);
        const objeto = {
            position: `${this.formGroup.value.diaAsistencia}`, 
            name: `${this.formGroup.value.horaEntrada}${this.formGroup.value.horaEntradaDia}`, 
            weight: `${this.formGroup.value.horaSalida}${this.formGroup.value.horaSalidaDia}`, 
            symbol: `${this.formGroup.value.horasTrabajadas}`, 
            coment: `${this.formGroup.value.comentario}`
        };
        ELEMENT_DATA.push(objeto);
        this.table.renderRows();
    }

    tipoRegistro(filterValue: string): void {
        if (filterValue.trim().toLowerCase() == '2') {
            this.registroMultiple = true;
        }else{
            this.registroMultiple = false;
        }
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {

    }
}
