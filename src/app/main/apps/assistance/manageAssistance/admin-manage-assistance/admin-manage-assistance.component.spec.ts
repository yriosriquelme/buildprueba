import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManageAssistanceComponent } from './admin-manage-assistance.component';

describe('AdminManageAssistanceComponent', () => {
  let component: AdminManageAssistanceComponent;
  let fixture: ComponentFixture<AdminManageAssistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminManageAssistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminManageAssistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
