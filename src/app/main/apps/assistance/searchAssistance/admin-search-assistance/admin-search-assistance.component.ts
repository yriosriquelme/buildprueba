import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { SelectionModel } from '@angular/cdk/collections';
import { Observable } from 'rxjs';

import { AssistanceController } from 'app/common/rest/services/AssistanceController';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { AssistanceTo, RequestCanonical, ResponseCanonical } from 'app/common/rest/services/Beans';


@Component({
  selector: 'app-admin-search-assistance',
  templateUrl: './admin-search-assistance.component.html',
  styleUrls: ['./admin-search-assistance.component.scss']
})
export class AdminSearchAssistanceComponent implements OnInit {
  horaEntrada = '00:00 AM';
  horaSalida = '00:00 AM';

  asistenciaUpdatearSalida: AssistanceTo;
  ultimoRegistro: string;

  centered = false;
  disabledEntrada = false;
  disabledSalida = false;
  unbounded = false;

  radius: number;
  color: string;

  // Para el reloj
  fecha: any;
  horas: any;
  ampm: any;
  minutos: any;
  segundos: any;
  diaSemana: any;
  dia: any;
  mes: any;
  year: any;

  // Para NgModels de reloj
  pHoras: any; // = document.getElementById('horas'),
  pAMPM: any; // = document.getElementById('ampm'),
  pMinutos: any; // = document.getElementById('minutos'),
  pSegundos: any; // = document.getElementById('segundos'),
  pDiaSemana: any; // = document.getElementById('diaSemana'),
  pDia: any; // = document.getElementById('dia'),
  pMes: any; // = document.getElementById('mes'),
  pYear: any; // = document.getElementById('year');

  // Variables
  employeePk: number;

  ngOnInit(): void {
    const currentSession = JSON.parse(localStorage.getItem('currentSession'));
    const nombre: string = currentSession.username;
    this._employeeController.findByUsername(nombre).subscribe(
            data => {
                console.log(data);
                this.employeePk = data.employeePk;
                this.registroVerificacion();
                this.ultimoRegistroMetodo();
            },
            error => console.log(JSON.stringify(error))
    );
    this.actualizarHora();
    setInterval(() => {
      this.actualizarHora(); 
    }, 1000);

    
  }

  constructor(
    private _assistanceController: AssistanceController,
    private _employeeController: EmployeeController
  ) 
  {}

  actualizarHora(): void{
    // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
    this.fecha = new Date();
    this.horas = this.fecha.getHours();
    this.minutos = this.fecha.getMinutes(),
    this.segundos = this.fecha.getSeconds(),
    this.diaSemana = this.fecha.getDay(),
    this.dia = this.fecha.getDate(),
    this.mes = this.fecha.getMonth(),
    this.year = this.fecha.getFullYear();
  
      
    // Obtenemos el dia se la semana y lo mostramos
    const semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    this.pDiaSemana = semana[this.diaSemana];
  
    // Obtenemos el dia del mes
    this.pDia = this.dia;
  
    // Obtenemos el Mes y año y lo mostramos
    const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    this.pMes = meses[this.mes];
    this.pYear = this.year;
  
    // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM
  
    if (this.horas >= 12) {
      this.horas = this.horas - 12;
      this.ampm = 'PM';
    } else {
      this.ampm = 'AM';
    }
  
    // Detectamos cuando sean las 0 AM y transformamos a 12 AM
    if (this.horas == 0 ){
      this.horas = 12;
    }
  
    // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
    if (this.horas < 10){this.horas = '0' + this.horas;}
    this.pHoras = this.horas;
    this.pAMPM = this.ampm;
  
    // Minutos y Segundos
    if (this.minutos < 10){ this.minutos = "0" + this.minutos; }
    if (this.segundos < 10){ this.segundos = "0" + this.segundos; }
  
    this.pMinutos = this.minutos;
    this.pSegundos = this.segundos;
  }

  marcarEntrada(): void {
    if (this.disabledEntrada == true) {
      // No sucede nada por que ya esta registrado
    } else {
      this.horaEntrada = `${this.pHoras}:${this.pMinutos}:${this.pSegundos}:${this.pAMPM}`;
      const requestCanonical: RequestCanonical = {
        assistanceTo: {
          employeeFk: this.employeePk,
          assistanceDay: new Date(),
          entryTime: new Date()
        }
      };
      this._assistanceController.createAssistance(requestCanonical).subscribe(
        data => {
            console.log(data);
            this.disabledEntrada = true;
        },
        error => console.log(JSON.stringify(error))
      );
    }
    
  }

  marcarSalida(): void {
    if (this.disabledSalida == true) {
      // No sucede nada por que ya esta registrado
    } else {
      this.horaSalida = `${this.pHoras}:${this.pMinutos}:${this.pSegundos}:${this.pAMPM}`;
      this.asistenciaUpdatearSalida.depatureTime = new Date();
      const requestCanonical: RequestCanonical = {
        lstAssistanceTo: [
          this.asistenciaUpdatearSalida
        ]
      };
      // Pasar objeto completo sino se borra
      this._assistanceController.updateAssistance(requestCanonical).subscribe(
        data => {
          console.log(data);
          this.registroVerificacion();
        },
        error => console.log(JSON.stringify(error))
      );
    }
    
    
    
  }
  
  registroVerificacion() {
    const requestCanonical: RequestCanonical = {
      assistanceTo: {
        employeeFk: this.employeePk
      }
    };
    this._assistanceController.getLastAssistance(requestCanonical).subscribe(
      data => {
          if (data.assistanceTo == null) {
            this.disabledEntrada = false;
            this.disabledSalida = true;
            //console.log(data);
          } else {
            //console.log(data);
            this.disabledEntrada = true;
            this.disabledSalida = false;
            const assistanceTo: AssistanceTo = data.assistanceTo;
            assistanceTo.assistanceDay = new Date(assistanceTo.assistanceDay);
            this.horaEntrada = `${assistanceTo.assistanceDay.getHours()}:${assistanceTo.assistanceDay.getMinutes()}:${assistanceTo.assistanceDay.getSeconds()}`;
            this.asistenciaUpdatearSalida = data.assistanceTo;
          }
      },
      error => console.log(JSON.stringify(error))
    );
  }

  ultimoRegistroMetodo(): void{
    this.ultimoRegistro = `Registro Actual`;
    const requestCanonical: RequestCanonical = {
      assistanceTo: {
        employeeFk: this.employeePk
      }
    };
    this._assistanceController.getLastRegisteredAssistance(requestCanonical).subscribe(
      data => {
        //console.log(data);
        const ultimoRegistroPrevio = new Date(data.assistanceTo.depatureTime);
        // Obtenemos el dia se la semana y lo mostramos
        const semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        this.ultimoRegistro = 
        `${semana[ultimoRegistroPrevio.getDay()]}, ${ultimoRegistroPrevio.getDate()} de ${meses[ultimoRegistroPrevio.getMonth()]} del ${ultimoRegistroPrevio.getFullYear()}`;
      },
      error => console.log(JSON.stringify(error))
    );
  }

}
