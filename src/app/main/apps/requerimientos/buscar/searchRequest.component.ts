import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { AddEditDialogComponent } from './dialog/add-edit-dialog/add-edit-dialog.component';

@Component({
    selector     : 'search-request-request',
    templateUrl  : './searchRequest.component.html',
    styleUrls    : ['./searchRequest.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SearchRequestComponent implements OnInit, OnDestroy
{
    codigo: string = "";
    cliente: string = "";
    estado: string = "";
    indicador: string = "";
    sistema: string = "";

    periodicElement = {requerimiento: "", cliente: "", estado: "", 
        descripcion: "", fechaCreacion: "", indicador: "", centro: "",
        sistema: "", componente: "", detalle: "", desarrollo: 0, qa: 0,
        devops: 0, total: 0, lstDetHoras: []};

    clientes = [
        {cliente: 'BVL'},
        {cliente: 'CAVALI'}
      ];

    estados = [
        {estado: 'Abierto'},
        {estado: 'Cerrado'}
    ];

    indicadores = [
        {indicador: 'Inversión'},
        {indicador: 'Gasto'},
        {indicador: 'Garantía'}
    ];

    sistemas = [
        {sistema: 'Wari'},
        {sistema: 'Factrack'},
        {sistema: 'Pagarés'},
        {sistema: 'Jira'}
    ];

    objectSelect:any[]=[];
    rowChecked: boolean = false;
    modHidden: boolean = true;

    public formGroup: FormGroup;
    columnas: string[] = ['requerimiento', 'cliente', 'sistema', 'indicador', 'estado', 'avance', 'horas', 'select'];

    datos: MatTableDataSource<PeriodicElement>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    selection = new SelectionModel<PeriodicElement>(true, []);

    constructor(private formBuilder: FormBuilder, private router: Router,
        private rutaActiva: ActivatedRoute,  public dialog: MatDialog)
    {
        const dato = ELEMENT_DATA;
        this.datos = new MatTableDataSource(dato);
        this.rutaActiva.queryParams.subscribe(params => {
            this.periodicElement.requerimiento = params['requerimiento'];
            this.periodicElement.cliente = params['cliente'];
            this.periodicElement.estado = params['estado'];
            this.periodicElement.descripcion = params['descripcion'];
            this.periodicElement.fechaCreacion = params['fechaCreacion'];
            this.periodicElement.indicador = params['indicador'];
            this.periodicElement.centro = params['centro'];
            this.periodicElement.sistema = params['sistema'];
            this.periodicElement.componente = params['componente'];
            this.periodicElement.detalle = params['detalle'];
            this.periodicElement.desarrollo = params['desarrollo'];
            this.periodicElement.qa = params['qa'];
            this.periodicElement.devops = params['devops'];
            this.periodicElement.total = params['total'];
            this.periodicElement.lstDetHoras = params['lstDetHoras'];
        });
        if(this.periodicElement.requerimiento!="" && this.periodicElement.requerimiento!=null){
            this.datos.data.push(this.periodicElement);
        }
    }

    ngOnInit(): void
    {
        this.buildForm();
        this.datos.paginator = this.paginator;
        this.datos.sort = this.sort;
        this.datos.filterPredicate = this.getFilterPredicate();
    }

    regisMasivo(){
        this.router.navigate(['/apps/requerimientos/regis-masivo']);
    }

    aplicarFiltros(){
        this.applyFilter();
    }

    getFilterPredicate() {
        return (row: PeriodicElement, filters: string) => {
          const filterArray = filters.split('$');
          const requerimiento = filterArray[0];
          const cliente = filterArray[1];
          const estado = filterArray[2];
          const indicador = filterArray[3];
          const sistema = filterArray[4];
          const matchFilter = [];
          const columnCodigo = row.requerimiento;
          const columnCliente = row.cliente;
          const columnEstado = row.estado;
          const columnIndicador = row.indicador;
          const columnSistema = row.sistema;
          const customFilterDR = columnCodigo.toLowerCase().includes(requerimiento);
          const customFilterDC = columnCliente.toLowerCase().includes(cliente);
          const customFilterDE = columnEstado.toLowerCase().includes(estado);
          const customFilterDI = columnIndicador.toLowerCase().includes(indicador);
          const customFilterDS = columnSistema.toLowerCase().includes(sistema);
          matchFilter.push(customFilterDR);
          matchFilter.push(customFilterDC);
          matchFilter.push(customFilterDE);
          matchFilter.push(customFilterDI);
          matchFilter.push(customFilterDS);
          return matchFilter.every(Boolean);
        };
    }
    
    applyFilter(){
        const codigo = this.formGroup.get('codigo').value;
        const cliente = this.formGroup.get('cliente').value;
        const estado = this.formGroup.get('estado').value;
        const indicador = this.formGroup.get('indicador').value;
        const sistema = this.formGroup.get('sistema').value;

        this.codigo = codigo === "" ? '' : codigo;
        this.cliente = cliente === "" ? '' : cliente;
        this.estado = estado === "" ? '' : estado;
        this.indicador = indicador === "" ? '' : indicador;
        this.sistema = sistema === "" ? '' : sistema;

        const filterValue = this.codigo + '$' + this.cliente + '$' + this.estado + '$' + this.indicador + '$' + this.sistema;

        this.datos.filter = filterValue.trim().toLowerCase();
    }
    private buildForm() {
        const codigo = '';
        const cliente = '';
        const estado = '';
        const indicador = '';
        const sistema = '';

        this.formGroup = this.formBuilder.group({
            codigo: codigo,
            cliente: cliente,
            estado: estado,
            indicador: indicador,
            sistema: sistema
        });
     }

    nuevo(){
        this.router.navigate(['/apps/requerimientos/registrar']);
    }

    limpiarInputs(){
        this.buildForm();
        this.applyFilter();
    }

    opcion(i:number){
        switch(i){
            case 1 : {
                for (let i = 0; i < this.objectSelect.length; i++) {
                    this.objectSelect[i].estado = "Abierto";
                }
                break;
            }
            case 2 : {
                for (let i = 0; i < this.objectSelect.length; i++) {
                    this.objectSelect[i].estado = "Cerrado";
                }
                break;
            }
            case 3 : {
                if(this.objectSelect.length<2){
                    this.router.navigate(['/apps/requerimientos/registrar']);
                }
                break;
            } 
            default : {
                break;
            }
        }
    }

    checkValue(e: any, row: PeriodicElement, i: number){
        this.rowChecked=e.checked;
        this.valor(row,i);
        if(this.objectSelect.length<2&&this.objectSelect.length>0){
            this.modHidden = false;
        }else{
            this.modHidden = true;
        }
    }

    checkValue1(e: any){
        if(e.checked!=false&&(this.selection.select.length<2&&this.selection.select.length>0)){
            this.modHidden = false;
        }else{
            this.modHidden = true;
        }
    }

    valor(row: PeriodicElement, i:number){
        if((row!=null) && (i!=null && i!=-1) && (this.rowChecked!=false)){
            this.objectSelect.push(row);
        }else{
            this.objectSelect.splice(i,1);
        }
    }

    ngOnDestroy(): void
    {
        
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.datos.data.length;
        return numSelected === numRows;
    }

    deselectObject(){
        this.objectSelect=this.selection.selected;
        var object: any[] = [];
        this.objectSelect= object;
        this.selection.clear();
    }

    masterToggle() {
        this.isAllSelected() ? this.deselectObject() : this.datos.data.forEach(row => this.selection.select(row));
    }

    checkboxLabel(row?: PeriodicElement): string {
        if (!row) {
        return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.requerimiento + 1}`;
    }

    openDialog(operacion:string , element:PeriodicElement) {
        let dialogRef = this.dialog.open(AddEditDialogComponent, {
            height: '250px',
            width: '600px',
            data: { name: operacion,
                    element: element
                  },
            panelClass: 'my-panel',
            disableClose: true
          });
    
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    selectRow(element:PeriodicElement){
        this.router.navigate(['/apps/requerimientos/registrar']);
    }

}


export interface PeriodicElement {
    requerimiento: string;
    cliente: string;
    estado: string;
    descripcion: string;
    fechaCreacion: string;
    indicador: string;
    centro: string;
    sistema: string;
    componente: string;
    detalle: string;
    desarrollo: number;
    qa: number;
    devops: number;
    total: number;
    lstDetHoras: Array<PartElement>;
}

export interface PartElement{
    dia: string;
    detalle: string;
    hora: number;
    tipo: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {requerimiento: 'RQSIS18-1', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 10, qa: 2, devops: 8, total: 20, lstDetHoras:null},
    {requerimiento: 'RQSIS18-2', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Factrack', componente: "", detalle: "", desarrollo: 4, qa: 55, devops: 20, total: 79, lstDetHoras:null},
    {requerimiento: 'RQSIS18-3', cliente: 'CAVALI', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Pagarés', componente: "", detalle: "", desarrollo: 1, qa: 43, devops: 30, total: 20, lstDetHoras:null},
    {requerimiento: 'RQSIS18-4', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Jira', componente: "", detalle: "", desarrollo: 24, qa: 65, devops: 1, total: 20, lstDetHoras:null},
    {requerimiento: 'RQSIS18-5', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Garantía', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 3, qa: 20, devops: 10, total: 33, lstDetHoras:null},
    {requerimiento: 'RQSIS18-6', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Factrack', componente: "", detalle: "", desarrollo: 20, qa: 4, devops: 10, total: 34, lstDetHoras:null},
    {requerimiento: 'RQSIS18-7', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Pagarés', componente: "", detalle: "", desarrollo: 10, qa: 10, devops: 10, total: 30, lstDetHoras:null},
    {requerimiento: 'RQSIS18-8', cliente: 'CAVALI', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Jira', componente: "", detalle: "", desarrollo: 20, qa: 20, devops: 20, total: 60, lstDetHoras:null},
    {requerimiento: 'RQSIS18-9', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 2, qa: 22, devops: 1, total: 25, lstDetHoras:null},
    {requerimiento: 'RQSIS18-10', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Garantía', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 12, qa: 22, devops: 3, total: 36, lstDetHoras:null},
    {requerimiento: 'RQSIS18-11', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Factrack', componente: "", detalle: "", desarrollo: 12, qa: 33, devops: 2, total: 47, lstDetHoras:null},
    {requerimiento: 'RQSIS18-12', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 22, qa: 3, devops: 43, total: 68, lstDetHoras:null},
    {requerimiento: 'RQSIS18-13', cliente: 'CAVALI', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Jira', componente: "", detalle: "", desarrollo: 2, qa: 23, devops: 4, total: 29, lstDetHoras:null},
    {requerimiento: 'RQSIS18-14', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 22, qa: 1, devops: 44, total: 67, lstDetHoras:null},
    {requerimiento: 'RQSIS18-15', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Garantía', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 2, qa: 12, devops: 33, total: 47, lstDetHoras:null},
    {requerimiento: 'RQSIS18-16', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Factrack', componente: "", detalle: "", desarrollo: 1, qa: 2, devops: 3, total: 6, lstDetHoras:null},
    {requerimiento: 'RQSIS18-17', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Inversión', centro: "", 
    sistema: 'Pagarés', componente: "", detalle: "", desarrollo: 2, qa: 3, devops: 4, total: 9, lstDetHoras:null},
    {requerimiento: 'RQSIS18-18', cliente: 'CAVALI', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Jira', componente: "", detalle: "", desarrollo: 3, qa: 5, devops: 3, total: 11, lstDetHoras:null},
    {requerimiento: 'RQSIS18-19', cliente: 'BVL', estado: 'Abierto', 
    descripcion: "", fechaCreacion: "", indicador: 'Gasto', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 2, qa: 1, devops: 1, total: 4, lstDetHoras:null},
    {requerimiento: 'RQSIS18-20', cliente: 'CAVALI', estado: 'Cerrado', 
    descripcion: "", fechaCreacion: "", indicador: 'Garantía', centro: "", 
    sistema: 'Wari', componente: "", detalle: "", desarrollo: 1, qa: 1, devops: 3, total: 5, 
    lstDetHoras:[
        {dia: "21-04-20", detalle: "Desarrollo Hallazgo Mejora", hora: 3, tipo: "QA"},
        {dia: "28-08-20", detalle: "	Desarrollo bdd", hora: 4, tipo: "Gest conf"}
    ]}
];