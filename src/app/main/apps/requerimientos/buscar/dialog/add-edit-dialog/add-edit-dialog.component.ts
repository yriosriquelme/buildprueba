import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  name: string;
  element: PeriodicElement;
}

export interface PeriodicElement {
  requerimiento: string;
  cliente: string;
  estado: string;
  descripcion: string;
  fechaCreacion: string;
  indicador: string;
  centro: string;
  sistema: string;
  componente: string;
  detalle: string;
  desarrollo: number;
  qa: number;
  devops: number;
  total: number;
}

@Component({
  selector: 'app-add-edit-dialog',
  templateUrl: './add-edit-dialog.component.html',
  styleUrls: ['./add-edit-dialog.component.scss']
})
export class AddEditDialogComponent implements OnInit {
  operation: any;
  horas:number = 0;

  constructor(
    public dialogRef: MatDialogRef<AddEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.operation = this.data;
  }

  ngOnInit() {
    this.horas = this.operation.element.total;
    console.log(this.operation);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  editarGuardar(){
    console.log(this.horas);
    this.operation.element.total=this.horas;
    console.log(this.operation.element.total);
    this.dialogRef.close();
  }

}
