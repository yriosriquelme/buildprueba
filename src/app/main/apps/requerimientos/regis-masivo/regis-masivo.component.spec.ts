import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisMasivoComponent } from './regis-masivo.component';

describe('RegisMasivoComponent', () => {
  let component: RegisMasivoComponent;
  let fixture: ComponentFixture<RegisMasivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisMasivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisMasivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
