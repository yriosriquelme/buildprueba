import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Label, MultiDataSet, Color } from 'ng2-charts';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.scss']
})
export class ReporteComponent implements OnInit {

  periodo: string = "";
  periodo1: string = "";
  empresa: string = "";
  indicador: string = "";
  sistema: string = "";

  periodos = [
    { periodo: '2019-1' },
    { periodo: '2019-2' },
    { periodo: '2020-1' },
    { periodo: '2020-2' }
  ];

  periodos1 = [
    { periodo: '2019-1' },
    { periodo: '2019-2' },
    { periodo: '2020-1' },
    { periodo: '2020-2' }
  ];

  empresas = [
    { empresa: 'BVL' },
    { empresa: 'CAVALI' }
  ];

  estados = [
    { estado: 'Abierto' },
    { estado: 'Cerrado' }
  ];

  indicadores = [
    { indicador: 'Inversión' },
    { indicador: 'Gasto' },
    { indicador: 'Garantía' }
  ];

  sistemas = [
    { sistema: 'Wari' },
    { sistema: 'Factrack' },
    { sistema: 'Pagarés' },
    { sistema: 'Jira' }
  ];

  radios = [
    { radio: 'Mayor a menor' },
    { radio: 'Menor a mayor' }
  ];

  //1.1
  widget5SelectedDay = 'today';


  lineChartchartType = 'line';
  lineChartDataChange: any[] = [];
  lineChartdatasets = [{
    'yesterday' : [{
      data: [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
      label: 'Visitors',
      fill: 'start'
    },
    {
      data: [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
      label: 'Page views',
      fill: 'start'
    }],
    'today':[{
      data: [410, 380, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
      label: 'Visitors',
      fill: 'start'
    },
    {
      data: [3000, 3400, 4100, 3800, 2200, 3200, 2900, 1900, 2900, 3900, 2500, 3800],
      label: 'Page Views',
      fill: 'start'
    }]
  }];
  /*
  {
      'yesterday' : [
        {
          label: 'Visitors',
          data: [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
          fill: 'start'

        },
        {
          label: 'Page views',
          data: [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
          fill: 'start'
        }
      ],
      'today' : [
        {
          label: 'Visitors',
          data: [410, 380, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
          fill: 'start'
        },
        {
          label: 'Page Views',
          data: [3000, 3400, 4100, 3800, 2200, 3200, 2900, 1900, 2900, 3900, 2500, 3800],
          fill: 'start'

        }
      ]
    }
  */
  lineChartlabels = ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'];
  lineChartcolors = [
    {
      borderColor: '#3949ab',
      backgroundColor: '#3949ab',
      pointBackgroundColor: '#3949ab',
      pointHoverBackgroundColor: '#3949ab',
      pointBorderColor: '#ffffff',
      pointHoverBorderColor: '#ffffff'
    },
    {
      borderColor: 'rgba(30, 136, 229, 0.87)',
      backgroundColor: 'rgba(30, 136, 229, 0.87)',
      pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
      pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
      pointBorderColor: '#ffffff',
      pointHoverBorderColor: '#ffffff'
    }
  ];
  lineChartoptions = {
    spanGaps: false,
    legend: {
      display: false
    },
    maintainAspectRatio: false,
    tooltips: {
      position: 'nearest',
      mode: 'index',
      intersect: false
    },
    layout: {
      padding: {
        left: 24,
        right: 32
      }
    },
    elements: {
      point: {
        radius: 4,
        borderWidth: 2,
        hoverRadius: 4,
        hoverBorderWidth: 2
      }
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false
          },
          ticks: {
            fontColor: 'rgba(0,0,0,0.54)'
          }
        }
      ],
      yAxes: [
        {
          gridLines: {
            tickMarkLength: 16
          },
          ticks: {
            stepSize: 1000
          }
        }
      ]
    },
    plugins: {
      filler: {
        propagate: false
      }
    }
  }

  //1
  lineChartData: ChartDataSets[] = [
    { data: [85, 72, 78, 75, 77, 75], label: 'Crude oil prices' },
  ];

  lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June'];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';


  //2
  doughnutChartLabels: Label[] = ['Frontend', 'Backend', 'API', 'Issues'];
  doughnutChartData: MultiDataSet = [[20, 30, 50, 40]];
  doughnutChartColors: Color[] = [{ backgroundColor: ['#ff4000', '#ff00bf', '#00ffff', '#ff0066'] }];
  doughnutChartType: ChartType = 'doughnut';

  //4
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['Apple', 'Banana', 'Kiwifruit', 'Blueberry', 'Orange', 'Grapes'];
  barChartData: ChartDataSets[] = [{ data: [55, 60, 65, 70, 75, 80], label: 'Pandora Technologies' }];
  barChartColors: Color[] = [{ backgroundColor: ['#ff0000', '#ff8000', '#ffbf00', '#ffff00', '#bfff00', '#80ff00'] }];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  public formGroup: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.widget5SelectedDay = 'today';
    this.lineChartDataChange = this.lineChartdatasets[0].today;
    //console.log(this.lineChartdatasets[0].yesterday);
    //console.log(this.lineChartdatasets[0].today);
    this.buildForm();
  }

  private buildForm() {
    const periodo = '';
    const empresa = '';
    const indicador = '';
    const sistema = '';
    this.formGroup = this.formBuilder.group({
      periodo: periodo,
      empresa: empresa,
      indicador: indicador,
      sistema: sistema
    });
  }

  getYesyerday(){
    this.widget5SelectedDay = 'yesterday';
    this.lineChartDataChange = this.lineChartdatasets[0].yesterday;
  }

  getToday(){
    this.widget5SelectedDay = 'today';
    this.lineChartDataChange = this.lineChartdatasets[0].today;
  }

  exportar(){
    
  }
}
