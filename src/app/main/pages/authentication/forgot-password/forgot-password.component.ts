import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Auth } from 'aws-amplify';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {
    changeConfirmPasswordForm: FormGroup;
    email: string;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private rutaActiva: ActivatedRoute,
        private router: Router
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this.rutaActiva.queryParams.subscribe(params => {
            this.email = params['email'];
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.changeConfirmPasswordForm = this._formBuilder.group({
            codeverify: ['', [Validators.required]],
            newpassword: ['', [Validators.required]],
            confirmnewpassword: ['', [Validators.required]]
        });
    }

    buildForm() {
        const codeverify = '';
        const newpassword = '';
        const confirmnewpassword = '';
        this.changeConfirmPasswordForm = this._formBuilder.group({
            codeverify: codeverify,
            newpassword: newpassword,
            confirmnewpassword: confirmnewpassword
        });
    }

    resendCode(): void {
        console.log(this.email);
        Auth.forgotPassword('juancho')
            .then((user) => {
                console.log(user);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    changeConfirmPassword() {
        const code = this.changeConfirmPasswordForm.get('codeverify').value;
        const password = this.changeConfirmPasswordForm.get('newpassword').value;
        const confirmpassword = this.changeConfirmPasswordForm.get('confirmnewpassword').value;
        if (password != confirmpassword) {
            console.error("Password different");
        }
        else {
            Auth.forgotPasswordSubmit('juancho', code, password)
                .then((user) => {
                    console.log(user);
                    this.router.navigate(['/pages/auth/login-2']);
                })
                .catch((err) => {
                    console.log(err);
                });
            this.buildForm();
        }
    }
}
