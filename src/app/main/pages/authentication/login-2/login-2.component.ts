import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

import { Auth } from 'aws-amplify';
import { Router } from '@angular/router';

import { ParameterController } from 'app/common/rest/services/ParameterController';
import { LoginController } from 'app/common/rest/services/LoginController';
import { EmployeeController } from 'app/common/rest/services/EmployeeController';
import { ParameterTo, RequestCanonical, ResponseCanonical } from 'app/common/rest/services/Beans';


@Component({
    selector: 'login-2',
    templateUrl: './login-2.component.html',
    styleUrls: ['./login-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Login2Component implements OnInit {
    requestCanonical: any = {};
    loginForm: FormGroup;
    responseCanonical: ResponseCanonical;
    lstParameter: ParameterTo;
    tipeDocument: any = [{}];
    tipeEstado: any = [{}];
    tipeMarket: any = [{}];
    lstStateVacation: any = [{}];
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private router: Router,
        private _parameterCtrl: ParameterController,
        private _loginCtrl: LoginController,
        private _employeeCtrl: EmployeeController,
        private _lstipeDocument: ParameterController,
        private _lstEstado: ParameterController,
        private _lstMarket: ParameterController,
        private _employeeController: EmployeeController
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            name: ['', Validators.required],
            password: ['', Validators.required]
        });

        const a: any = {};
        a.lstMaster = [null, null];
        a.lstParameter = [null, null];
        a.masterTo = {};
        a.message = '';
        a.parameterTo = {
            //parameterPk:1,
            //name:'juan',
            registerUser: 'YRIOS',
            //order:2,
            //codeCategory:'3',
            masterFk: 1,
            state: 1
        };
        a.state = '';
        /*this._parameterCtrl.getListMaster(a).subscribe(
            data => {
                console.log(data);
                
            },
            error => console.log(JSON.stringify(error))
        );*/
        /*this._parameterCtrl.getListParameters(a).subscribe(
            data => {
                console.log(data);
                
            },
            error => console.log(JSON.stringify(error))
        );*/
        /*this._parameterCtrl.updateParameter(a).subscribe(
            data => {
                console.log(data);
                
            },
            error => console.log(JSON.stringify(error))  
        );*/
        /* this._parameterCtrl.createParameter(a).subscribe(
             data => {
 
                 console.log(data);
                 
             },
             error => console.log(JSON.stringify(error))
         );*/
        const employe: any = {};
        employe.lstMaster = [null, null];
        employe.lstParameter = [null, null];
        employe.masterTo = {};
        employe.message = '';
        employe.parameterTo = {};
        employe.state = '';
        // PARA UPDATE SE NECESITA EMPLOYEEPK SINO CREA OTRO COMO INSERT
        employe.employeeTo = {
            employeeStatus: 1,
            name: 'Pamela Cristina',
            lastName: 'Quispe Mendieta'
        };
        // FUNCIONA LISTA EMPLEADOS
        /*this._employeeCtrl.getListEmployee(employe).subscribe(
            data => {
                console.log(data);
                
            },
            error => console.log(JSON.stringify(error))
            
        );*/

        // FUNCIONA CREATE
        /*this._employeeCtrl.createEmployee(employe).subscribe(
            data => {
                console.log(data);
                
            },
            error => console.log(JSON.stringify(error))
            
        );*/
        // FUNCIONA UPDATE
        /*this._employeeCtrl.updateEmployee(employe).subscribe(
            data => {
                console.log(data);
                
            },
            error => console.log(JSON.stringify(error))
            
        );*/
       /* const doc: any = {};
        doc.lstMaster = [null, null];
        doc.lstParameter = [null, null];
        doc.masterTo = {};
        doc.message = '';
        doc.parameterTo = {
            masterFk: 1,
            state: 1
        };
        doc.state = '';
        this._lstipeDocument.getListParameters(doc).subscribe(
            data => {
                this.lstParameter = data.lstParameter;

                this.tipeDocument = this.lstParameter;
                //el parameter pk lo mando al document type del empleado

                console.log(this.tipeDocument);
            },
            error => console.log(JSON.stringify(error))
        );*/

    }

    signIn() {
        const username = this.loginForm.get('name').value;
        const password = this.loginForm.get('password').value;
        Auth.signIn({ username: this.loginForm.get('name').value, password: this.loginForm.get('password').value })
            .then((user) => {
                console.log(user);
                Auth.currentSession().then((session) => {
                    const currentSession = session.getAccessToken().decodePayload();
                    localStorage.setItem('currentSession', JSON.stringify(currentSession));
                    console.log(currentSession);
                    const a: any = {};
                    a.lstMaster = [null, null];
                    a.lstParameter = [null, null];
                    a.masterTo = {};
                    a.message = '';
                    a.parameterTo = {};
                    a.state = '';
                    a.employeeTo = {
                        username: username,
                        password: password
                    };
                    a.assistanceTo = {};
                    a.lstAssistanceTo = [null];
                    /*this._loginCtrl.validateLogin(a).subscribe(
                        data => {
                            console.log(data);
                            this.responseCanonical = data;
                            console.log(this.responseCanonical);
                            localStorage.setItem('currentUser', JSON.stringify(this.responseCanonical.employeeTo));

                        },
                        error => console.log(JSON.stringify(error))
                    );*/
                })
                    .catch((err) => {
                        console.log(err);
                    });

                const doc: any = {};
                doc.lstMaster = [null, null];
                doc.lstParameter = [null, null];
                doc.masterTo = {};
                doc.message = '';
                doc.parameterTo = {
                    masterFk: 1,
                    state: 1
                };
                doc.state = '';
                this._lstipeDocument.getListParameters(doc).subscribe(
                    data => {
                        this.lstParameter = data.lstParameter;

                        this.tipeDocument = this.lstParameter;
                        console.log(this.tipeDocument);
                        //el parameter pk lo mando al document type del empleado
                        localStorage.setItem('currentDoc', JSON.stringify(this.tipeDocument));

                    },
                    error => console.log(JSON.stringify(error))
                );


                const estado: any = {};
                estado.lstMaster = [null, null];
                estado.lstParameter = [null, null];
                estado.masterTo = {};
                estado.message = '';
                estado.parameterTo = {
                    masterFk: 7,
                    state: 1
                };
                estado.state = '';
                this._lstEstado.getListParameters(estado).subscribe(
                    data => {
                        this.lstParameter = data.lstParameter;
                        this.tipeEstado = this.lstParameter;
                        console.log(this.tipeEstado);
                        localStorage.setItem('currentState', JSON.stringify(this.tipeEstado));
                    },
                    error => console.log(JSON.stringify(error))
                );
                const puesto: any = {};
                puesto.lstMaster = [null, null];
                puesto.lstParameter = [null, null];
                puesto.masterTo = {};
                puesto.message = '';
                puesto.parameterTo = {
                    masterFk: 6,
                    state: 1
                };
                puesto.state = '';
                this._lstMarket.getListParameters(puesto).subscribe(
                    data => {
                        this.lstParameter = data.lstParameter;
                        this.tipeMarket = this.lstParameter;
                        console.log(this.tipeMarket);
                        localStorage.setItem('currentMarket', JSON.stringify(this.tipeMarket));
                    },
                    error => console.log(JSON.stringify(error))
                );
                
                const stateVacation: any = {};
                stateVacation.lstMaster = [null, null];
                stateVacation.lstParameter = [null, null];
                stateVacation.masterTo = {};
                stateVacation.message = '';
                stateVacation.parameterTo = {
                    masterFk: 4,
                    state: 1
                };
                stateVacation.state = '';
                this._lstEstado.getListParameters(stateVacation).subscribe(
                    data => {
                        this.lstParameter = data.lstParameter;
                        this.lstStateVacation = this.lstParameter;
                        console.log(this.lstStateVacation);
                        localStorage.setItem('currentStateVacation', JSON.stringify(this.lstStateVacation));
                    },
                    error => console.log(JSON.stringify(error))
                );
                Auth.currentAuthenticatedUser().then((data) => {
                    const currentUser = data;
                    // localStorage.setItem('currentUser', JSON.stringify(currentUser));
                    console.log(data);
                })
                    .catch((err) => {
                        console.log(err);
                    });
                this.router.navigate(['/apps/assistance/searchAssistance/adminSearchAssistanceComponent']);
            })
            .catch((err) => {
                console.log(err);
            });
    }
}