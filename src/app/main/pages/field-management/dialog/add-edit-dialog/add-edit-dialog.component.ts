import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-add-edit-dialog',
  templateUrl: './add-edit-dialog.component.html',
  styleUrls: ['./add-edit-dialog.component.scss']
})
export class AddEditDialogComponent implements OnInit {
  contactForm: FormGroup;
  operation: any;

  createFormGroup(): any{
    return new FormGroup({
      numDoc: new FormControl(''),
      codemp: new FormControl(0)
    });
  }

  constructor(
    public dialogRef: MatDialogRef<AddEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.operation = this.data;
      this.contactForm = this.createFormGroup();
  }

  ngOnInit() {
    console.log(this.operation);
  }

  onResetForm(): void{
    this.contactForm.reset();
  }

  onSaveForm(): void{
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
