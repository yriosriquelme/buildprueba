import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder } from '@angular/forms';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import {MatDialog} from '@angular/material/dialog';

import { AddEditDialogComponent } from 'app/main/pages/field-management/dialog/add-edit-dialog/add-edit-dialog.component';
import { ConfirmationDialogComponent } from 'app/main/pages/field-management/dialog/confirmation-dialog/confirmation-dialog.component';
/*
export interface PeriodicElement {
  name: string;
  fecha: string;
  edit: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {name: 'Ada Lovelace', fecha: 'December 10, 1815', edit: 'edit'},
  {name: 'Grace Hopper', fecha: 'December 9, 1906', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'},
  {name: 'Margaret Hamilton', fecha: 'August 17, 1936', edit: 'edit'},
  {name: 'Joan Clarke', fecha: 'June 24, 1917', edit: 'edit'}
];
*/
// Nuevo paginacion y pruebas de filtro
export interface UserData {
  name: string;
  progress: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
// Nuevo paginacion y pruebas de filtro

@Component({
  selector: 'app-field-management',
  templateUrl: './field-management.component.html',
  styleUrls: ['./field-management.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class FieldManagementComponent implements OnInit , OnDestroy{
  plans = [
    {name: 'amelia'},
    {name: 'mia'}
  ];
  /*public formGroup: FormGroup;
  columnas: string[] = ['name', 'fecha', 'edit'];
  datos = ELEMENT_DATA;
  */
  // Nuevo paginacion y filtrado
    displayedColumns: string[] = ['name', 'progress'];
    dataSource: MatTableDataSource<UserData>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
  // Nuevo paginacion y filtrado

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog) { 
    // Nuevo paginacion y filtrado
    // Create 100 users
    const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
    // Nuevo paginacion y filtrado
  }

  ngOnInit(): void {
    // this.buildForm();
    // Nuevo paginacion y filtrado
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // Nuevo paginacion y filtrado
  }

  openDialog(operation: string, description_name: string) {
    // =====================================
    const dialogRef = this.dialog.open(AddEditDialogComponent, {
      // width: '250px',
      data: {name: operation, animal: description_name},
      height: '250px',
      width: '600px',
      panelClass: 'my-panel',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  openConfirmation() {
    // =====================================
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      // width: '250px',
      data: 'hola',
      height: '230px',
      width: '400px',
      panelClass: 'my-panel',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  // Nuevo paginacion y filtrado
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  // Nuevo paginacion y filtrado

  /*private buildForm() {
    const dateLength = 10;
    const today = new Date().toISOString().substring(0, dateLength);
    const empleado = '1';*/
    /*const name = 'CLINSMAN CAMPOS';*/
    /*this.formGroup = this.formBuilder.group({
        empleado: name.toLowerCase()
        empleado: empleado,
        diaAsistencia: today
    });
  }*/

  ngOnDestroy(): void {

  }

}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    name: name,
    progress: Math.round(Math.random() * 100).toString()
  };
}
