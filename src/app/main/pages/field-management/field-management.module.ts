import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';


import { FuseCountdownModule } from '@fuse/components';

import { FieldManagementComponent } from 'app/main/pages/field-management/field-management.component';

import { MatPaginatorModule } from '@angular/material';
import { AddEditDialogComponent } from './dialog/add-edit-dialog/add-edit-dialog.component';
import { ConfirmationDialogComponent } from './dialog/confirmation-dialog/confirmation-dialog.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';

const routes: Routes = [
    {
        path     : 'field-management',
        component: FieldManagementComponent
    }
];

@NgModule({
    declarations: [
        FieldManagementComponent,
        AddEditDialogComponent,
        ConfirmationDialogComponent,
    ],
    entryComponents: [AddEditDialogComponent, ConfirmationDialogComponent],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatTabsModule,
        MatTableModule,
        MatToolbarModule,
        MatCardModule,

        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),
        ChartsModule,
        NgxChartsModule,

        FuseSharedModule,
        FuseWidgetModule,
        MatPaginatorModule
    ]
})
export class FieldManagementModule
{
}

